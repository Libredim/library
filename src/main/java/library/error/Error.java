package library.error;

public enum Error {
    EMAIL("email is incorrect"),
    PASSWORD("password is incorrect"),
    PASSWORD_EQUALITY("passwords not equal"),
    AUTHOR_NAME("author name is invalid"),
    BOOK_NAME("book name is invalid"),
    USERNAME("Username incorrect"),
//    LAST_NAME("last name incorrect"),

    PERIODICAL_NAME("periodical name incorrect"),
    PERIODICAL_COST("cost format incorrect"),

    ENTRY_NAME("entry name incorrect"),
    ENTRY_TEXT("entry text incorrect"),

    PUBLISHER_NAME("publisher name incorrect"),
    MAX_COST("max cost incorrect"),


    EMAIL_OCCUPIED("email occupied"),
    EMAIL_PASSWORD_MISMATCH("e-mail and password combination not found"),
    USERNAME_NOT_FOUND("username not found"),
    EMAIL_NOT_FOUND("e-mail not found");


    private String message;

    private Error(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}