package library.model.dao;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public interface GenericDao<E> {
    Optional<E> find(int id);

    default List<E> findAll(){
        return Collections.emptyList();
    }

    E insert(E e) throws SQLException;

    E update(E e) throws SQLException;

    E delete(int id) throws SQLException;


}

