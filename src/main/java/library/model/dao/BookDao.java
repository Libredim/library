package library.model.dao;

import library.controller.dto.CatalogPageInput;
import library.model.dao.entity.Book;
import library.model.dao.service.BookService;

import java.util.List;
import java.util.Optional;

public interface BookDao extends GenericDao<Book> {

    Optional<Book> findByName(String name);


    List<Book> findPageOfBooks(CatalogPageInput input, BookService service);

    List<Book> search(String search);

}
