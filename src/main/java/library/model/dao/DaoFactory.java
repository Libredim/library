package library.model.dao;


import library.model.dao.jdbc.JdbcDaoFactory;

import java.sql.Connection;

public abstract class DaoFactory {

    public void startTransaction() {

    }

    private static class Holder {
        private static final DaoFactory JDBC_DAO_FACTORY = JdbcDaoFactory.getInstance();
    }

    public static DaoFactory getInstance() {
        return Holder.JDBC_DAO_FACTORY;
    }

    public abstract Connection getConnection();


    public abstract UserDao createUserDao(Connection connection);

    public abstract BookDao createJdbcBookDao(Connection connection);

    public abstract OrderDao createJdbcOrderDao(Connection connection);

    public abstract AbonnementDao createJdbcAbonnementDao(Connection connection);

//    public AbonnementDao createJdbcAbonnementDao() {
//        try (Connection connection = getConnection()) {
//            return createJdbcAbonnementDao(connection);
//        }catch (Exception e){
//            rollback(connection);
//            throw new RuntimeException();
//        }
//    }

    public abstract AuthorDao createJdbcAuthorDao(Connection connection);

    public abstract PublisherDao createJdbcPublisherDao(Connection connection);

}
