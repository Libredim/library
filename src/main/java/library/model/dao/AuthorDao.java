package library.model.dao;

import library.model.dao.entity.Author;

public interface AuthorDao extends GenericDao<Author> {

}
