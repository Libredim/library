package library.model.dao;


import library.model.dao.entity.User;
import library.model.dao.service.UserService;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface UserDao extends GenericDao<User> {
    Optional<User> findByMail(String mail);

    Optional<List<User>> findPageOfUsers(Integer page, Integer usersOnPage, UserService service);

    void topUpUser(User user, double cash) throws SQLException;

    void banUser(int userId) throws SQLException;

    void unbanUser(int userId) throws SQLException;

    void deleteLibrarian(int userId) throws SQLException;


    void addBookToReadingRoom(User user, int bookId) throws SQLException;

}
