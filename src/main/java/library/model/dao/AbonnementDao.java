package library.model.dao;

import library.controller.dto.ReturnBookInput;
import library.model.dao.entity.Abonnement;
import library.model.dao.entity.User;

import java.sql.SQLException;

public interface AbonnementDao extends GenericDao<Abonnement> {

    void returnBook(ReturnBookInput input) throws SQLException;

    void payAndReturnBook(User user, ReturnBookInput input) throws SQLException;
}
