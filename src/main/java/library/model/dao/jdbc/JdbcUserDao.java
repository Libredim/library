package library.model.dao.jdbc;

import library.model.dao.UserDao;
import library.model.dao.entity.Role;
import library.model.dao.entity.User;
import library.model.dao.exceptions.EmailOccupiedException;
import library.model.dao.service.UserService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JdbcUserDao implements UserDao {

    private static final String GET_NUMBER_OF_ROWS = "SELECT COUNT(*) FROM user";
    public static final String FIND_USERS_FOR_PAGE = "select * from (select * from user limit ?,?)as user inner join  user_has_role ON user.id=user_id ";
    public static final String SELECT_FROM_USERS_BY_ID = "select * from (select * from user where id=3)as user inner join  user_has_role ON user.id=user_id " +
            "left join user_has_book on user.id=user_has_book.user_id";
    public static final String SELECT_FROM_USERS_BY_EMAIL = "select * from (select * from user where email=?)as user inner join  user_has_role ON user.id=user_id " +
            "left join user_has_book on user.id=user_has_book.user_id";
    public static final String SET_BOOK_TO_READING_ROOM = "UPDATE book SET amount = amount-1 WHERE id = ?";
    public static final String SELECT_FROM_USERS_BY_USERNAME = "select * from (select * from user where username=?)as user inner join  user_has_role ON user.id=user_id";
    public static final String INSERT_USER = "INSERT INTO user(username,email,password) VALUES(?,?,?)";
    public static final String TOP_UP = "UPDATE user SET account  = account+? WHERE id = ?";
    public static final String BAN_USER = "UPDATE user SET ban  =1 WHERE id = ?";
    public static final String UNBAN_USER = "UPDATE user SET ban  =0 WHERE id = ?";
    public static final String DELETE_LIBRARIAN = "DELETE FROM user WHERE id=?";
    public static final String TAKE_BOOK_TOREADING_HOLE = "INSERT INTO user_has_book (user_id,book_id) VALUES(?,?)";
    private static final String SELECT_FROM_ROLES_BI_ID = "SELECT * FROM user_has_role where user_id=?;";
    private static final String INSERT_USER_TO_ROLE = "INSERT INTO user_has_role(role_id,user_id) VALUES ((select id from role where name=?) , ?)";
    private static final String ROWS = "COUNT(*)";
    private static final String ID_COL = "id";
    private static final String USERNAME_COL = "username";
    private static final String EMAIL_COL = "email";
    private static final String PASSWORD_COL = "password";
    private static final String ROLE_COL = "role";
    private static final String CREATE_DATE_COL = "create_time";
    public static final String ROLE_ID = "role_id";
    public static final String BAN = "ban";
    public static final String ACCOUNT = "account";
    public static final String BOOK_ID = "book_id";
    Connection connection;

    public JdbcUserDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Optional<User> find(int id) {
        Optional<User> result = Optional.empty();
        try (PreparedStatement query =
                     connection.prepareStatement(SELECT_FROM_USERS_BY_ID)) {
            query.setInt(1, id);
            ResultSet resultSet = query.executeQuery();
            if (resultSet.next()) {
                result = Optional.of(extractUserFromResultSet(resultSet, query));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }


    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User insert(User user) {
        try {
            PreparedStatement query =
                    connection.prepareStatement(INSERT_USER,
                            Statement.RETURN_GENERATED_KEYS);
            query.setString(1, user.getUsername());
            query.setString(2, user.getEmail());
            query.setString(3, user.getPassword());
            query.executeUpdate();
            ResultSet keys = query.getGeneratedKeys();
            if (keys.next()) {
                user.setId(keys.getInt(1));

            }
            query.close();
            query = connection.prepareStatement(SELECT_FROM_USERS_BY_ID);
            query.setString(1, user.getStringId());
            keys = query.executeQuery();
            if (keys.next()) {
                user.setCreateDate(keys.getDate(CREATE_DATE_COL));
            }
            query.close();


            query = connection.prepareStatement(INSERT_USER_TO_ROLE);
            query.setString(1, user.getRole().get(0).toString());

            query.setString(2, user.getStringId());

            query.executeUpdate();
            query.close();

        } catch (SQLException e) {
            if (e instanceof SQLIntegrityConstraintViolationException) {
                throw new EmailOccupiedException(e, user);
            } else {
                throw new RuntimeException(e);
            }
        }
        return user;
    }


    @Override
    public User update(User user) {
        return null;
    }

    @Override
    public User delete(int id) {
        return null;
    }

    @Override
    public Optional<User> findByMail(String mail) {
        Optional<User> result = Optional.empty();
        try (PreparedStatement query =
                     connection.prepareStatement(SELECT_FROM_USERS_BY_EMAIL)) {
            query.setString(1, mail);
            ResultSet resultSet = query.executeQuery();
            if (resultSet.next()) {
                result = Optional.of(extractUserFromResultSet(resultSet, query));

            }
        } catch (SQLException e) {

            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public Optional<List<User>> findPageOfUsers(Integer page, Integer usersOnPage, UserService service) {
        List<User> list;
        try {

            PreparedStatement query =
                    connection.prepareStatement(GET_NUMBER_OF_ROWS);

            ResultSet resultSet = query.executeQuery();
            if (resultSet.next()) {
                int max = resultSet.getInt(ROWS);
                if (max % usersOnPage == 0) {
                    service.setMaxPage(max / usersOnPage);
                } else service.setMaxPage(1 + (max / usersOnPage));

            }
            resultSet.close();
            query.close();

            int offset = (page - 1) * usersOnPage;
            query = connection.prepareStatement(FIND_USERS_FOR_PAGE);
            query.setInt(1, offset);
            query.setInt(2, usersOnPage);


            resultSet = query.executeQuery();

            list = extractUsersFromResultSet(resultSet);

            query.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return Optional.of(list);
    }

    @Override
    public void topUpUser(User user, double cash) throws SQLException {
        PreparedStatement query = connection.prepareStatement(TOP_UP);
        query.setDouble(1, cash);
        query.setInt(2, user.getId());
        query.executeUpdate();
        query.close();

    }

    @Override
    public void banUser(int userId) throws SQLException {
        PreparedStatement query = connection.prepareStatement(BAN_USER);
        query.setInt(1, userId);
        query.executeUpdate();
        query.close();
    }

    @Override
    public void unbanUser(int userId) throws SQLException {
        PreparedStatement query = connection.prepareStatement(UNBAN_USER);
        query.setInt(1, userId);
        query.executeUpdate();
        query.close();
    }

    @Override
    public void deleteLibrarian(int userId) throws SQLException {
        PreparedStatement query = connection.prepareStatement(DELETE_LIBRARIAN);
        query.setInt(1, userId);
        query.executeUpdate();
        query.close();
    }

    @Override
    public void addBookToReadingRoom(User user, int bookId) throws SQLException {
        PreparedStatement query = connection.prepareStatement(SET_BOOK_TO_READING_ROOM);
        query.setInt(1, bookId);
        query.executeUpdate();
        query.close();


        query = connection.prepareStatement(TAKE_BOOK_TOREADING_HOLE);
        query.setInt(1, user.getId());
        query.setInt(2, bookId);
        query.executeUpdate();
        query.close();
    }

    private List<User> extractUsersFromResultSet(ResultSet resultSet) throws SQLException {

        List<User> result = new ArrayList<>();
        while (resultSet.next()) {
            if (!result.isEmpty() && (result.get(result.size() - 1).getId() == resultSet.getInt(ID_COL))) {
                result.get(result.size() - 1).addRole(Role.getRole(resultSet.getInt(ROLE_ID)).toString());
            } else {
                result.add(new User.Builder()
                        .setId(resultSet.getInt(ID_COL))
                        .setUsername(resultSet.getString(USERNAME_COL))
                        .setBan(resultSet.getBoolean(BAN))
                        .setRole(Role.getRole(resultSet.getInt(ROLE_ID)).toString())
                        .setEmail(resultSet.getString(EMAIL_COL))
                        .setPassword(resultSet.getString(PASSWORD_COL))
                        .setCreateDate(resultSet.getDate(CREATE_DATE_COL))
                        .setAccount(resultSet.getDouble(ACCOUNT))
                        .build());

            }
        }
        return result;

    }

    private User extractUserFromResultSet(ResultSet rs, PreparedStatement query) throws SQLException {

        User user = new User.Builder()
                .setUsername(rs.getString(USERNAME_COL))
                .setId(rs.getInt(ID_COL))
                .setPassword(rs.getString(PASSWORD_COL))
                .setEmail(rs.getString(EMAIL_COL))
                .setCreateDate(rs.getDate(CREATE_DATE_COL))
                .setRole(Role.getRole(rs.getInt(ROLE_ID)).toString())
                .setBan(rs.getBoolean(BAN))
                .setAccount(rs.getDouble(ACCOUNT))
                .setBookInReadingRoom(rs.getInt(BOOK_ID)).build();

        while (rs.next()) {
            user.addRole(Role.getRole(rs.getInt(ROLE_ID)).toString());

        }


        return user;
    }


}
