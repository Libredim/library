package library.model.dao.jdbc;

import library.model.dao.DaoFactory;
import library.model.dao.PublisherDao;
import library.model.dao.UserDao;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class JdbcDaoFactory extends DaoFactory {

    private DataSource dataSource;

    private JdbcDaoFactory() {
        try {
            InitialContext ctx = new InitialContext();
            Context envContext = (Context) ctx.lookup("java:/comp/env");
            dataSource = (DataSource) envContext.lookup("jdbc/mydb");
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            throw new RuntimeException(e);
        }
    }

    @Override
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            throw new RuntimeException(e);
        }
    }

    @Override
    public UserDao createUserDao(Connection connection) {
        return new JdbcUserDao(connection);
    }

    public JdbcBookDao createJdbcBookDao(Connection connection) {
        return new JdbcBookDao(connection);
    }

    public JdbcOrderDao createJdbcOrderDao(Connection connection) {
        return new JdbcOrderDao(connection);
    }

    public JdbcAbonnementDao createJdbcAbonnementDao(Connection connection) {
        return new JdbcAbonnementDao(connection);
    }

    public JdbcAuthorDao createJdbcAuthorDao(Connection connection) {
        return new JdbcAuthorDao(connection);
    }

    @Override
    public PublisherDao createJdbcPublisherDao(Connection connection) {
        return new JdbcPublisherDao(connection);
    }

    private static class Holder {

        private static final JdbcDaoFactory JDBC_DAO_FACTORY = new JdbcDaoFactory();
    }

    public static JdbcDaoFactory getInstance() {
        return Holder.JDBC_DAO_FACTORY;
    }
}



