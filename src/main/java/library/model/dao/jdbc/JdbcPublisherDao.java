package library.model.dao.jdbc;

import library.model.dao.PublisherDao;
import library.model.dao.entity.Publisher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JdbcPublisherDao implements PublisherDao {
    public static final String SELECT_ALL = "SELECT * FROM publisher";

    public static final String ID = "id";
    public static final String NAME = "name";
    Connection connection;

    public JdbcPublisherDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Optional find(int id) {
        return Optional.empty();
    }

    @Override
    public List<Publisher> findAll() {
        List<Publisher> publishers;
        try (PreparedStatement query = connection.prepareStatement(SELECT_ALL)) {
            ResultSet rs = query.executeQuery();
            publishers = new ArrayList<>();
            while (rs.next()) {
                publishers.add(new Publisher.Builder()
                        .setId(rs.getInt(ID))
                        .setName(rs.getString(NAME))
                        .build());
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return publishers;
    }

    @Override
    public Object insert(Object o) throws SQLException {
        return null;
    }

    @Override
    public Object update(Object o) {
        return null;
    }

    @Override
    public Object delete(int id) throws SQLException {
        return null;
    }
}
