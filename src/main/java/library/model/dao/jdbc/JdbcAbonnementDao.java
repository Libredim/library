package library.model.dao.jdbc;

import library.controller.dto.ReturnBookInput;
import library.model.dao.AbonnementDao;
import library.model.dao.entity.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class JdbcAbonnementDao implements AbonnementDao {

    public static final String SELECT_ABONNEMENT_BY_ID = "SELECT user_id, abonement.book_id, exp_date, fine, start_date, Status_id, book.name, publisher_id, amount, publication_date,publisher.name, author_id, author.name FROM ((select * from  abonement_has_book where user_id =?) as abonement  inner JOIN book ON abonement.book_id=id inner JOIN publisher ON book.publisher_id=publisher.id inner JOIN author ON author_id=author.id)";
    public static final String DELETE_FROM_CURRENT = "DELETE FROM abonement_has_book WHERE  user_id=? and book_id=? and exp_date=? and start_date=? and fine=? and Status_id=(select id from status where name=?)";
    public static final String ADD_TO_STORY = "INSERT INTO abonement_story (user_id,book_id,exp_date,start_date, fine,Status_id) VALUES (?, ?, ?,?, ?, (select id from status where name=?))";
    public static final String INCREASE_BOOK_AMOUNT_BY_ID = "UPDATE book SET amount = amount+1 WHERE id = ?";
    public static final String PAY_FINE = "UPDATE user SET account  = account-? WHERE id = ?";

    public static final String BOOK_id = "abonement.book_id";
    public static final String USER_ID = "user_id";
    public static final String EXP_DATE = "exp_date";
    public static final String FINE = "fine";
    public static final String START_DATE = "start_date";
    public static final String STATUS = "Status_id";
    public static final String BOOK_NAME = "book.name";
    public static final String PUBLISHER_ID = "publisher_id";
    public static final String AMOUNT = "amount";

    public static final String PUBLICATION_DATE = "publication_date";

    public static final String PUBLISHER_NAME = "publisher.name";
    public static final String AUTHOR = "author_id";
    public static final String AUTHOR_NAME = "author.name";

    Connection connection;

    public JdbcAbonnementDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Optional<Abonnement> find(int id) {

        Optional<Abonnement> result = Optional.empty();
        try (PreparedStatement query = connection.prepareStatement(SELECT_ABONNEMENT_BY_ID)) {
            query.setInt(1, id);
            ResultSet resultSet = query.executeQuery();
            result = getAbonnementFromResultSet(resultSet);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return result;

    }


    @Override
    public List<Abonnement> findAll() {
        return Collections.emptyList();
    }

    @Override
    public Abonnement insert(Abonnement abonnement) {
        return null;
    }

    @Override
    public Abonnement update(Abonnement abonnement) {
        return null;
    }

    @Override
    public Abonnement delete(int id) {
        return null;
    }

    private Optional<Abonnement> getAbonnementFromResultSet(ResultSet resultSet) throws SQLException {
        Abonnement result = new Abonnement();

        while (resultSet.next()) {

            result.setId(resultSet.getInt(USER_ID));

            result.addToDetailsMap(
                    new AbonnementDetails.Builder()
                            .setAbbonement(result)
                            .setExpDate(resultSet.getDate(EXP_DATE))
                            .setStartDate(resultSet.getDate(START_DATE))
                            .setFine(resultSet.getDouble(FINE))
                            .setStatus(Status.getStatus(resultSet.getInt(STATUS)))
                            .addBook(new Book.Builder()

                                    .setPublicationDate(resultSet.getDate(PUBLICATION_DATE))
                                    .setPublisher(resultSet.getInt(PUBLISHER_ID), resultSet.getString(PUBLISHER_NAME))
                                    .setAmount(resultSet.getInt(AMOUNT))
                                    .setName(resultSet.getString(BOOK_NAME))
                                    .setId(resultSet.getInt(BOOK_id))
                                    .setAuthor(resultSet.getInt(AUTHOR), resultSet.getString(AUTHOR_NAME))
                                    .build()
                            ).build()

            );


        }

        return Optional.of(result);
    }

    private void moveDetailsToStory() {

    }

    @Override
    public void returnBook(ReturnBookInput input) throws SQLException {
        PreparedStatement query = connection.prepareStatement(DELETE_FROM_CURRENT);
        query.setInt(1, input.getUserId());
        query.setInt(2, input.getBookId());
        query.setString(3, input.getExpDate());
        query.setString(4, input.getStartDate());
        query.setDouble(5, input.getFine());
        query.setString(6, input.getStatus());

        if (query.executeUpdate() == 1) {

            query.close();
            query = connection.prepareStatement(ADD_TO_STORY);
            query.setInt(1, input.getUserId());
            query.setInt(2, input.getBookId());
            query.setString(3, input.getExpDate());
            query.setString(4, input.getStartDate());
            query.setDouble(5, input.getFine());
            query.setString(6, Status.RETURNED.toString());
            query.executeUpdate();
            query.close();
        } else throw new SQLException();

        query = connection.prepareStatement(INCREASE_BOOK_AMOUNT_BY_ID);
        query.setInt(1, input.getBookId());
        query.executeUpdate();

        query.close();
    }

    @Override
    public void payAndReturnBook(User user, ReturnBookInput input) throws SQLException {
        if (user.getId() != input.getUserId()) throw new RuntimeException();
        PreparedStatement query = connection.prepareStatement(PAY_FINE);

        query.setDouble(1, input.getFine());
        query.setInt(2, user.getId());
        query.executeUpdate();
        query.close();


        query = connection.prepareStatement(DELETE_FROM_CURRENT);
        query.setInt(1, input.getUserId());
        query.setInt(2, input.getBookId());
        query.setString(3, input.getExpDate());
        query.setString(4, input.getStartDate());
        query.setDouble(5, input.getFine());
        query.setString(6, input.getStatus());

        if (query.executeUpdate() == 1) {

            query.close();
            query = connection.prepareStatement(ADD_TO_STORY);
            query.setInt(1, input.getUserId());
            query.setInt(2, input.getBookId());
            query.setString(3, input.getExpDate());
            query.setString(4, input.getStartDate());
            query.setDouble(5, input.getFine());
            query.setString(6, Status.RETURNED_WITH_FINE.toString());
            query.executeUpdate();
            query.close();
        } else throw new SQLException();

        query = connection.prepareStatement(INCREASE_BOOK_AMOUNT_BY_ID);
        query.setInt(1, input.getBookId());
        query.executeUpdate();

        query.close();
    }
}
