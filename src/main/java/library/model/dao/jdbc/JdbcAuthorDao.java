package library.model.dao.jdbc;

import library.model.dao.AuthorDao;
import library.model.dao.entity.Author;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JdbcAuthorDao implements AuthorDao {
    public static final String SELECT_ALL = "SELECT * FROM author";

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String INSERT_AUTHOR = "INSERT INTO author (name) VALUES(?)";


    Connection connection;

    public JdbcAuthorDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Optional<Author> find(int id) {
        return Optional.empty();
    }

    @Override
    public List<Author> findAll() {
        List<Author> authors;
        try (PreparedStatement query = connection.prepareStatement(SELECT_ALL)) {
            ResultSet rs = query.executeQuery();
            authors = new ArrayList<>();
            while (rs.next()) {
                authors.add(new Author.Builder()
                        .setId(rs.getInt(ID))
                        .setName(rs.getString(NAME))
                        .build());
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return authors;
    }

    @Override
    public Author insert(Author author) throws SQLException {
        PreparedStatement query = connection.prepareStatement(INSERT_AUTHOR);
        query.setString(1, author.getName());
        query.executeUpdate();
        return author;

    }

    @Override
    public Author update(Author author) {
        return null;
    }

    @Override
    public Author delete(int id) throws SQLException {
        return null;
    }
}
