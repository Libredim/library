package library.model.dao.jdbc;

import library.controller.dto.CatalogPageInput;
import library.model.dao.BookDao;
import library.model.dao.entity.Book;
import library.model.dao.service.BookService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JdbcBookDao implements BookDao {

    private static final String GET_NUMBER_OF_ROWS = "SELECT COUNT(*) FROM book";


    public static final String SELECT_BOOK_BY_ID = "SELECT book.id, book.name, publisher_id, amount, publication_date,  author_id,  publisher.name,   author.name " +
            "FROM ((SELECT * from book where id=?) as book left JOIN publisher ON book.publisher_id=publisher.id inner JOIN author ON author_id=author.id)";
    public static final String DELETE_BOOK = "DELETE FROM book WHERE id=?";
    public static final String SEARCH = "SELECT book.id, book.name, publisher_id, amount, publication_date,  author_id,  publisher.name,   author.name FROM book " +
            "left join author on Author_id=author.id inner join publisher on publisher_id=publisher.id " +
            "where book.name like ? or author.name  like ? order by id";
    public static final String UPDATE_BOOK = "UPDATE book SET  amount =?, publication_date = ?, name = ?, Author_id =?, publisher_id = ? WHERE id = ?";
    public static final String INSERT_BOOK = "INSERT INTO book ( amount, publication_date, name, Author_id, publisher_id) VALUES (?, ?, ?, ?, ?)";
    private static final String ROWS = "COUNT(*)";
    public static final String BOOK_ID = "book.id";
    public static final String BOOK_NAME = "book.name";

    public static final String AMOUNT = "amount";
    public static final String PUBLICATION_DATE = "publication_date";
    public static final String PUBLISHER = "publisher_id";
    public static final String PUBLISHER_NAME = "publisher.name";

    public static final String AUTHOR = "author_id";
    public static final String AUTHOR_NAME = "author.name";


    Connection connection;

    public JdbcBookDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Optional<Book> findByName(String name) {
        return Optional.empty();
    }


    @Override
    public List<Book> findPageOfBooks(CatalogPageInput input, BookService service) {

        List<Book> result = new ArrayList<>();
        Integer offset = input.getBooksOnPage() * (input.getPage() - 1);
        try {
            PreparedStatement query =
                    connection.prepareStatement(GET_NUMBER_OF_ROWS);

            ResultSet resultSet = query.executeQuery();
            if (resultSet.next()) {
                int max = resultSet.getInt(ROWS);
                if (max % input.getBooksOnPage() == 0) {
                    service.setMaxPage(max / input.getBooksOnPage());
                } else service.setMaxPage(1 + (max / input.getBooksOnPage()));
            }
            resultSet.close();
            query.close();


            query = connection.prepareStatement(input.geSql());

            query.setInt(1, offset);
            query.setInt(2, input.getBooksOnPage());


            resultSet = query.executeQuery();

            result = extractBooksFromResultSet(resultSet);

            query.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public List<Book> search(String search) {
        List<Book> result = new ArrayList<>();

        try (PreparedStatement query = connection.prepareStatement(SEARCH)) {


            query.setString(1, search);
            query.setString(2, search);
            ResultSet resultSet = query.executeQuery();

            result = extractBooksFromResultSet(resultSet);


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public Optional<Book> find(int id) {
        Optional<Book> result = Optional.empty();
        try {
            PreparedStatement query =
                    connection.prepareStatement(SELECT_BOOK_BY_ID);
            query.setInt(1, id);


            ResultSet resultSet = query.executeQuery();

            result = Optional.of(extractBookFromResultSet(resultSet));

            query.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public List<Book> findAll() {
        return null;
    }

    @Override
    public Book insert(Book book) throws SQLException {
        PreparedStatement query = connection.prepareStatement(INSERT_BOOK, Statement.RETURN_GENERATED_KEYS);
        query.setInt(1, book.getAmount());
        query.setDate(2, new Date(book.getPublicationDate().getTime()));
        query.setString(3, book.getName());
        query.setInt(4, book.getAuthor().getId());
        query.setInt(5, book.getPublisher().getId());

        query.executeUpdate();
        ResultSet keys = query.getGeneratedKeys();
        if (keys.next()) {
            book.setId(keys.getInt(1));

        }
        query.close();

        return book;
    }


    @Override
    public Book update(Book book) throws SQLException {
        PreparedStatement query = connection.prepareStatement(UPDATE_BOOK);
        query.setInt(1, book.getAmount());
        query.setDate(2, new Date(book.getPublicationDate().getTime()));
        query.setString(3, book.getName());
        query.setInt(4, book.getAuthor().getId());
        query.setInt(5, book.getPublisher().getId());
        query.setInt(6, book.getId());
        query.executeUpdate();
        query.close();
        return find(book.getId()).get();
    }

    @Override
    public Book delete(int id) throws SQLException {

        try (PreparedStatement query = connection.prepareStatement(DELETE_BOOK)) {

            query.setInt(1, id);

            query.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException(e);
        }

        return null;
    }


    private List<Book> extractBooksFromResultSet(ResultSet resultSet) throws SQLException {
        List<Book> result = new ArrayList<>();
        while (resultSet.next()) {

            result.add(new Book.Builder()
                    .setId(resultSet.getInt(BOOK_ID))
                    .setName(resultSet.getString(BOOK_NAME))

                    .setAmount(resultSet.getInt(AMOUNT))
                    .setPublicationDate(resultSet.getDate(PUBLICATION_DATE))
                    .setPublisher(resultSet.getInt(PUBLISHER), resultSet.getString(PUBLISHER_NAME))
                    .setAuthor(resultSet.getInt(AUTHOR), resultSet.getString(AUTHOR_NAME))
                    .build());

        }

        return result;
    }

    private Book extractBookFromResultSet(ResultSet resultSet) throws SQLException {
        Book result = null;
        if (resultSet.next()) {


            result = new Book.Builder()
                    .setId(resultSet.getInt(BOOK_ID))
                    .setName(resultSet.getString(BOOK_NAME))

                    .setAmount(resultSet.getInt(AMOUNT))
                    .setPublicationDate(resultSet.getDate(PUBLICATION_DATE))
                    .setPublisher(resultSet.getInt(PUBLISHER), resultSet.getString(PUBLISHER_NAME))
                    .setAuthor(resultSet.getInt(AUTHOR), resultSet.getString(AUTHOR_NAME))
                    .build();


        }
        return result;
    }


}



