package library.model.dao.jdbc;

import library.model.dao.OrderDao;
import library.model.dao.entity.Book;
import library.model.dao.entity.Order;
import library.model.dao.entity.User;
import library.model.dao.service.OrderService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class JdbcOrderDao implements OrderDao {
    Connection connection;
    public static final String INSERT_ORDER = "INSERT INTO mydb.order (user_id, book_id, desired_date) VALUES (?,?,?)";
    private static final String GET_NUMBER_OF_ROWS = "SELECT COUNT(*) FROM mydb.order";
    public static final String GET_ORDERS_FOR_PAGE = "select user_id, book_id, desired_date, user.username, user.email, user.ban,  book.name,  book.amount  from ((SELECT * from mydb.order order by user_id limit ?,?) as receipt left Join user on receipt.user_id=user.id left join  book on receipt.book_id=book.id)";
    public static final String APPROVE_ORDER = "INSERT INTO abonement_has_book (user_id, book_id, exp_date, fine, start_date, status_id) VALUES (?,?,?, 100.5,(SELECT CURDATE()), 1)";
    public static final String FIND_ORDER_BY_USER_ID = "select * from mydb.order where user_id=?";
    public static final String SET_BOOK_TO_ABONNEMENT = "UPDATE book SET amount = amount-1 WHERE id = ?";
    public static final String DELETE_BY_USER_ID = "DELETE FROM mydb.order WHERE user_id=?";
    private static final String ROWS = "COUNT(*)";
    public static final String USER_ID = "user_id";
    public static final String BOOK_ID = "book_id";
    public static final String DESIRED_DATE = "desired_date";
    public static final String USERNAME = "user.username";
    public static final String EMAIL = "user.email";
    public static final String USER_BAN = "user.ban";
    public static final String BOOK_NAME = "book.name";

    public static final String BOOK_AMOUNT = "book.amount";


    public JdbcOrderDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Optional<Order> find(int id) {
        Optional<Order> order = Optional.empty();
        try (PreparedStatement query = connection.prepareStatement(FIND_ORDER_BY_USER_ID)) {
            query.setInt(1, id);
            ResultSet rs = query.executeQuery();

            if (rs.next()) order = Optional.ofNullable(new Order.Builder()
                    .setUser(new User.Builder().setId(rs.getInt(USER_ID)).build())
                    .setBook(new Book.Builder().setId(rs.getInt(BOOK_ID)).build())
                    .setDesiredDate(rs.getDate(DESIRED_DATE)).build());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return order;
    }

    @Override
    public List<Order> findAll() {
        return null;
    }

    @Override
    public Order insert(Order order) {
        try (PreparedStatement query =
                     connection.prepareStatement(INSERT_ORDER)) {
            query.setInt(1, (int)
                    order.getUser().getId());
            query.setInt(2, order.getBook().getId());
            query.setDate(3, new Date(order.getDesiredDate().getTime()));
            query.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;
    }

    @Override
    public Order update(Order order) {
        return null;
    }

    @Override
    public Order delete(int id) throws SQLException {
        PreparedStatement query = connection.prepareStatement(DELETE_BY_USER_ID);
        Order order = find(id).get();
        query.setInt(1, id);
        query.executeUpdate();
        query.close();
        return order;


    }

    @Override
    public Optional<List<Order>> findPageOfOrders(int page, int ordersOnPage, OrderService orderService) {
        List<Order> list;
        try {

            PreparedStatement query =
                    connection.prepareStatement(GET_NUMBER_OF_ROWS);

            ResultSet resultSet = query.executeQuery();
            if (resultSet.next()) {
                int max = resultSet.getInt(ROWS);
                if (max % ordersOnPage == 0) {
                    orderService.setMaxPage(max / ordersOnPage);
                } else orderService.setMaxPage(1 + (max / ordersOnPage));

            }
            resultSet.close();
            query.close();
            int offset = (page - 1) * ordersOnPage;

            query = connection.prepareStatement(GET_ORDERS_FOR_PAGE);
            query.setInt(1, offset);
            query.setInt(2, ordersOnPage);


            resultSet = query.executeQuery();

            list = extractOrdersFromResultSet(resultSet);

            query.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return Optional.of(list);
    }

    @Override
    public void approveOrder(Order order) throws SQLException {
        PreparedStatement query = connection.prepareStatement(SET_BOOK_TO_ABONNEMENT);
        query.setInt(1, order.getBook().getId());
        query.executeUpdate();
        query.close();
        query = connection.prepareStatement(APPROVE_ORDER);
        query.setInt(1, order.getUser().getId());
        query.setInt(2, order.getBook().getId());
        query.setDate(3, (Date) order.getDesiredDate());

        query.executeUpdate();
        query.close();

    }


    private List<Order> extractOrdersFromResultSet(ResultSet resultSet) throws SQLException {
        List<Order> list = new ArrayList<>();
        while (resultSet.next()) {
            list.add(
                    new Order.Builder().setUser(
                                    new User.Builder()
                                            .setId(resultSet.getInt(USER_ID))
                                            .setBan(resultSet.getBoolean(USER_BAN))
                                            .setUsername(resultSet.getString(USERNAME))
                                            .setEmail(EMAIL).build()
                            )
                            .setBook(new Book.Builder()
                                    .setId(resultSet.getInt(BOOK_ID))
                                    .setAmount(resultSet.getInt(BOOK_AMOUNT))
                                    .setName(resultSet.getString(BOOK_NAME))
                                    .build()
                            )
                            .setDesiredDate(resultSet.getDate(DESIRED_DATE)).build()
            );
        }
        return list;

    }
}
