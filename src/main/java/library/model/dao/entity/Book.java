package library.model.dao.entity;

import java.util.Date;
import java.util.Objects;
import java.util.Set;

public class Book {
    private int id;
    private String name;
    private Publisher publisher;
    private int amount;
    private Date publicationDate;

    private Author author;
    private Set<AbonnementDetails> abonements;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", nameUA='" + name + '\'' +
                ", publisher=" + publisher +
                ", amount=" + amount +
                ", publicationDate=" + publicationDate +

                ", author=" + author +
                '}';
    }


    public boolean addAbonement(AbonnementDetails ad) {
        return abonements.add(ad);
    }

    public static class Builder {
        private Book instance = new Book();

        public Builder setId(int id) {
            instance.id = id;
            return this;
        }

        public Builder setName(String name) {
            instance.name = name;
            return this;
        }


        public Builder setPublisher(int id, String name) {
            if (Publisher.PUBLISHERS.get(id) != null) {
                instance.publisher = Publisher.PUBLISHERS.get(id);
            } else {

                instance.publisher = new Publisher.Builder()
                        .setId(id)
                        .setName(name).build();

            }
            return this;
        }

        public Builder setAmount(int amount) {
            instance.amount = amount;
            return this;
        }

        public Builder setPublicationDate(Date publicationDate) {
            instance.publicationDate = publicationDate;
            return this;
        }

        public Builder setAuthor(int id, String name) {
            {

                instance.setAuthor(new Author.Builder()
                        .setId(id)

                        .setName(name).build());

            }
            return this;
        }


        public Book build() {


            return instance;
        }
    }
}


