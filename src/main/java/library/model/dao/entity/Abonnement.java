package library.model.dao.entity;

import java.util.HashMap;
import java.util.Map;

public class Abonnement {
    private int id;
    Map<Integer, AbonnementDetails> detailsMap;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Map<Integer, AbonnementDetails> getDetailsMap() {
        return detailsMap;
    }

    public void addToDetailsMap(AbonnementDetails details) {
        detailsMap.put(details.getBook().getId(), details);
    }

    public Abonnement() {
        detailsMap = new HashMap<>();
    }

    public static class Builder {
        private Abonnement instance = new Abonnement();

        public Builder setId(int id) {
            instance.id = id;
            return this;
        }

        public Builder addDetails(AbonnementDetails ad) {
            instance.addToDetailsMap(ad);
            return this;
        }


        public Abonnement build() {
            return instance;
        }
    }

    @Override
    public String toString() {
        return "Abonnement{" +
                "id=" + id +
                ", detailsMap=" + detailsMap +
                '}';
    }
}
