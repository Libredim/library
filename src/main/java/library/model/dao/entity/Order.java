package library.model.dao.entity;

import java.util.Date;

public class Order {
    User user;
    Book book;
    Date desiredDate;

    public Order() {

    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Date getDesiredDate() {
        return desiredDate;
    }

    public void setDesiredDate(Date desiredDate) {
        this.desiredDate = desiredDate;
    }

    @Override
    public String toString() {
        return "Order{" +
                "user=" + user +
                ", book=" + book +
                ", desiredDate=" + desiredDate +
                '}';
    }

    public static class Builder {
        private Order instance = new Order();

        public Builder setUser(User user) {
            instance.user = user;
            return this;
        }

        public Builder setBook(Book book) {
            instance.book = book;
            return this;
        }

        public Builder setDesiredDate(Date date) {
            instance.desiredDate = date;
            return this;
        }

        public Order build() {


            return instance;
        }

    }
}
