package library.model.dao.entity;

import java.util.ArrayList;
import java.util.List;

public class AbonementStory {
    List<AbonnementDetails> story;

    public AbonementStory() {
        story = new ArrayList<>();
    }

    public List<AbonnementDetails> getStory() {
        return story;
    }

    public void addToStory(AbonnementDetails details) {
        story.add(details);
    }
}
