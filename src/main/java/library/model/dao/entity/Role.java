package library.model.dao.entity;

public enum Role {
    GUEST, USER, ADMIN, LIBRARIAN;

    public static Role getRole(int id) {
        switch (id) {

            case 2:
                return Role.USER;

            case 3:
                return Role.ADMIN;

            case 4:
                return Role.LIBRARIAN;

            default:
                return Role.GUEST;

        }


    }
}
