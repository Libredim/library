package library.model.dao.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User {
    private int id;
    private String username;
    private String email;
    private String password;
    private Date createDate;
    private List<String> role = new ArrayList<>();
    private Abonnement abonnement;
    private Book bookInReadingRoom;
    private boolean ban;
    private double account;

    public boolean isBan() {
        return ban;
    }

    public double getAccount() {
        return account;
    }

    public void setAccount(double account) {
        this.account = account;
    }

    public void setBan(boolean ban) {
        this.ban = ban;
    }

    public Book getBook() {
        return bookInReadingRoom;
    }

    public void setBook(Book book) {
        this.bookInReadingRoom = book;
    }


    private User() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public List<String> getRole() {
        return role;
    }

    public String getStringId() {
        String res = "" + id;
        return res.trim();

    }

    public Book getBookInReadingRoom() {
        return bookInReadingRoom;
    }

    public void setBookInReadingRoom(Book bookInReadingRoom) {
        this.bookInReadingRoom = bookInReadingRoom;
    }

    public void addRole(String role) {
        this.role.add(role);
    }

    public Abonnement getAbonnement() {
        return abonnement;
    }

    public void setAbonnement(Abonnement abonnement) {
        this.abonnement = abonnement;
    }

    public static class Builder {
        private User instance = new User();

        public Builder setId(int id) {
            instance.id = id;
            return this;
        }

        public Builder setUsername(String username) {
            instance.username = username;
            return this;
        }

        public Builder setEmail(String email) {
            instance.email = email;
            return this;
        }

        public Builder setPassword(String password) {
            instance.password = password;
            return this;
        }

        public Builder setRole(String role) {
            instance.addRole(role);
            return this;
        }

        public Builder setCreateDate(Date createDate) {
            instance.createDate = createDate;
            return this;
        }

        public Builder setAbonement(Abonnement abonnement) {
            instance.abonnement = abonnement;
            return this;
        }

        public Builder setAccount(double account) {
            instance.account = account;
            return this;
        }

        public Builder setBan(boolean ban) {
            instance.ban = ban;
            return this;
        }

        public Builder setBookInReadingRoom(int id) {
            if (id < 1) instance.bookInReadingRoom = null;
            else instance.bookInReadingRoom = new Book.Builder().setId(id).build();
            return this;
        }


        public User build() {
            return instance;
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", createDate=" + createDate +
                ", role=" + role +
                ", abonnement=" + abonnement +
                ", book=" + bookInReadingRoom +
                ", account " + account +
                '}';
    }
}
