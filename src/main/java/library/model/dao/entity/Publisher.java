package library.model.dao.entity;

import java.util.HashMap;
import java.util.Map;

public class Publisher {
    private int id;
    private String name;

    @Override
    public String toString() {
        return "Publisher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public static final Map<Integer, Publisher> PUBLISHERS = new HashMap<>();

    public Publisher() {

    }

    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public static class Builder {
        private Publisher instance = new Publisher();

        public Publisher.Builder setId(int id) {
            instance.id = id;
            return this;
        }

        public Builder setName(String name) {
            instance.name = name;
            return this;
        }


        public Publisher build() {
            PUBLISHERS.put(instance.getId(), instance);

            return instance;
        }

    }
}
