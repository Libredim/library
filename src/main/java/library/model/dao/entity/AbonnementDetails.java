package library.model.dao.entity;

import java.util.Date;

public class AbonnementDetails {
    Abonnement abonnement;
    Book book;
    private Date startDate;
    private Date expDate;
    private Double fine;
    private Status status;


    public AbonnementDetails() {


    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    public Double getFine() {
        return fine;
    }

    public void setFine(Double fine) {
        this.fine = fine;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean checkStatus() {
        return false;
    }

    public Abonnement getAbonnement() {
        return abonnement;
    }

    public void setAbonnement(Abonnement abonnement) {
        this.abonnement = abonnement;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }


    public static class Builder {
        private AbonnementDetails instance = new AbonnementDetails();

        public Builder setAbbonement(Abonnement abonnement) {
            instance.abonnement = abonnement;
            return this;
        }

        public Builder addBook(Book book) {
            instance.book = book;
            return this;
        }

        public Builder setStartDate(Date date) {
            instance.startDate = date;
            return this;
        }

        public Builder setExpDate(Date date) {
            instance.expDate = date;
            return this;
        }

        public Builder setStatus(Status status) {
            instance.status = status;
            return this;
        }

        public Builder setFine(double fine) {
            instance.fine = fine;
            return this;
        }

        public AbonnementDetails build() {
            return instance;
        }
    }

    @Override
    public String toString() {
        return "AbonnementDetails{" +
                "abonnement=" + abonnement.getId() +
                ", book=" + book +
                ", startDate=" + startDate +
                ", expDate=" + expDate +
                ", fine=" + fine +
                ", status=" + status +
                '}';
    }
}
