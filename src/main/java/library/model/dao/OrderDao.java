package library.model.dao;

import library.model.dao.entity.Order;
import library.model.dao.service.OrderService;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface OrderDao extends GenericDao<Order> {

    Optional<List<Order>> findPageOfOrders(int page, int ordersOnPage, OrderService orderService);

    void approveOrder(Order order) throws SQLException;

}
