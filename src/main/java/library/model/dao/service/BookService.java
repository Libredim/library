package library.model.dao.service;

import library.controller.dto.BookInput;
import library.controller.dto.CatalogPageInput;
import library.model.dao.BookDao;
import library.model.dao.DaoFactory;
import library.model.dao.entity.Book;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class BookService {
    private DaoFactory factory;

    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }


    private int maxPage;

    public void deleteBook(int bookId) {
        Connection connection = factory.getConnection();
        try {
            connection.setAutoCommit(false);
            BookDao dao = factory.createJdbcBookDao(connection);
            dao.delete(bookId);


            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Optional<Book> editBook(BookInput input) {
        Connection connection = factory.getConnection();
        Book book;
        try {
            connection.setAutoCommit(false);
            book = new Book.Builder()
                    .setId(input.getId())
                    .setAmount(input.getAmount())
                    .setName(input.getName())
                    .setAuthor(input.getAuthor(), null)
                    .setPublicationDate(extractDate(input.getDate()))
                    .setPublisher(input.getPublisher(), null).build();


            BookDao dao = factory.createJdbcBookDao(connection);
            book = dao.update(book);


            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return Optional.ofNullable(book);
    }

    public Optional<Book> createBook(BookInput input) {

        Connection connection = factory.getConnection();
        Book book;
        try {
            connection.setAutoCommit(false);
            book = new Book.Builder()

                    .setAmount(input.getAmount())
                    .setName(input.getName())
                    .setAuthor(input.getAuthor(), null)
                    .setPublicationDate(extractDate(input.getDate()))
                    .setPublisher(input.getPublisher(), null).build();


            BookDao dao = factory.createJdbcBookDao(connection);
            book = dao.insert(book);


            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return Optional.ofNullable(book);

    }

    public Optional<List<Book>> search(String search) {
        List<Book> list;
        try (Connection connection = factory.getConnection()) {
            BookDao bookDao = factory.createJdbcBookDao(connection);

            list = bookDao.search(search);
        } catch (SQLException e) {

            throw new RuntimeException(e);
        }

        return Optional.ofNullable(list);
    }

    private static class Holder {
        private static final BookService INSTANCE = new BookService(DaoFactory.getInstance());
    }

    public static BookService getInstance() {
        return Holder.INSTANCE;

    }

    BookService(DaoFactory factory) {
        this.factory = factory;

    }

    public Optional<Book> findBook(String name) {
        try (Connection connection = factory.getConnection()) {
            BookDao bookDao = factory.createJdbcBookDao(connection);

            return bookDao.findByName(name);
        } catch (SQLException e) {

            throw new RuntimeException(e);
        }

    }

    public Optional<List<Book>> findPageOfBooks(CatalogPageInput input) {
        try (Connection connection = factory.getConnection()) {
            BookDao bookDao = factory.createJdbcBookDao(connection);

            return Optional.of(bookDao.findPageOfBooks(input, this));
        } catch (SQLException e) {

            throw new RuntimeException(e);
        }

    }

    public Optional<Book> findBook(Integer id) {
        try (Connection connection = factory.getConnection()) {
            BookDao bookDao = factory.createJdbcBookDao(connection);

            return bookDao.find(id);
        } catch (SQLException e) {

            throw new RuntimeException(e);
        }

    }

    private void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private Date extractDate(String str) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
