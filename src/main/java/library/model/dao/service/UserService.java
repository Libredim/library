package library.model.dao.service;

import library.controller.dto.SubscribeInput;
import library.model.dao.DaoFactory;
import library.model.dao.UserDao;
import library.model.dao.entity.Book;
import library.model.dao.entity.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class UserService {
    private DaoFactory factory;

    //    private UserDetailsService userDetailsService;
    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }


    private int maxPage;

    public void userTopUp(User user, double cash) {
        Connection connection = factory.getConnection();

        try {
            connection.setAutoCommit(false);
            UserDao userDao = factory.createUserDao(connection);
            userDao.topUpUser(user, cash);


            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void banUser(int userId) {
        Connection connection = factory.getConnection();


        try {
            connection.setAutoCommit(false);
            UserDao userDao = factory.createUserDao(connection);
            userDao.banUser(userId);


            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void unbanUser(int userId) {
        Connection connection = factory.getConnection();


        try {
            connection.setAutoCommit(false);
            UserDao userDao = factory.createUserDao(connection);
            userDao.unbanUser(userId);


            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteLibrarian(int userId) {
        Connection connection = factory.getConnection();


        try {
            connection.setAutoCommit(false);
            UserDao userDao = factory.createUserDao(connection);
            userDao.deleteLibrarian(userId);


            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void takeBookToReadingRoom(User user, int bookId) {

        Connection connection = factory.getConnection();


        try {
            connection.setAutoCommit(false);
            UserDao userDao = factory.createUserDao(connection);
            userDao.addBookToReadingRoom(user, bookId);


            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        user.setBookInReadingRoom(new Book.Builder().setId(bookId).build());
    }

    private static class Holder {
        private static final UserService INSTANCE = new UserService(DaoFactory.getInstance());
    }

    public static UserService getInstance() {
        return Holder.INSTANCE;

    }

    UserService(DaoFactory factory) {
        this.factory = factory;

    }

    public Optional<User> findUser(String username) {
        try (Connection connection = factory.getConnection()) {
            UserDao userDao = factory.createUserDao(connection);

            return userDao.findByMail(username);
        } catch (SQLException e) {

            throw new RuntimeException(e);
        }

    }

    public Optional<List<User>> findPageOfUsers(Integer page, Integer usersOnPage) {
        Optional<List<User>> list = Optional.empty();
        try (Connection connection = factory.getConnection()) {
            UserDao userdao = factory.createUserDao(connection);

            list = userdao.findPageOfUsers(page, usersOnPage, this);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    public boolean checkPassword(User user, String password) {
        return password.equals(user.getPassword());

    }

    public User saveUser(SubscribeInput input) {
        Connection connection = factory.getConnection();
        User user = new User.Builder()
                .setEmail(input.getMail())
                .setUsername(input.getUsername())
                .setPassword(input.getPasswordFirst())
                .setRole(input.getRole())
                .build();
        try {
            connection.setAutoCommit(false);
            UserDao userDao = factory.createUserDao(connection);
            user = userDao.insert(user);
//            userDetailsService.saveDefaultUserDetails(user, connection);

            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    private void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }


}
