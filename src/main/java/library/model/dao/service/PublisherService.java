package library.model.dao.service;

import library.model.dao.DaoFactory;
import library.model.dao.PublisherDao;
import library.model.dao.entity.Publisher;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class PublisherService {

    DaoFactory factory;

    public Optional<List<Publisher>> findAll() {
        PublisherDao dao = factory.createJdbcPublisherDao(factory.getConnection());
        return Optional.ofNullable(dao.findAll());
    }

    private static class Holder {
        private static final PublisherService INSTANCE = new PublisherService(DaoFactory.getInstance());
    }

    public static PublisherService getInstance() {
        return PublisherService.Holder.INSTANCE;

    }

    PublisherService(DaoFactory factory) {
        this.factory = factory;

    }


    private void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
