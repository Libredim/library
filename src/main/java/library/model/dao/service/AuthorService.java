package library.model.dao.service;

import library.controller.dto.AuthorInput;
import library.model.dao.AuthorDao;
import library.model.dao.DaoFactory;
import library.model.dao.entity.Author;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class AuthorService {
    private DaoFactory factory;

    public void addAuthor(AuthorInput input) {
        Connection connection = factory.getConnection();
        Author author = new Author.Builder()
                .setName(input.getName()).build();
        try {
            connection.setAutoCommit(false);
            AuthorDao dao = factory.createJdbcAuthorDao(connection);
            author = dao.insert(author);
//            userDetailsService.saveDefaultUserDetails(user, connection);

            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Optional<List<Author>> findAll() {
        AuthorDao dao = factory.createJdbcAuthorDao(factory.getConnection());
        return Optional.ofNullable(dao.findAll());
    }

    private static class Holder {
        private static final AuthorService INSTANCE = new AuthorService(DaoFactory.getInstance());
    }

    public static AuthorService getInstance() {
        return AuthorService.Holder.INSTANCE;

    }

    AuthorService(DaoFactory factory) {
        this.factory = factory;

    }


    private void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
