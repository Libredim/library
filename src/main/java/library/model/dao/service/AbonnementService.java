package library.model.dao.service;

import library.controller.dto.ReturnBookInput;
import library.model.dao.AbonnementDao;
import library.model.dao.DaoFactory;
import library.model.dao.entity.Abonnement;
import library.model.dao.entity.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

public class AbonnementService {
    private DaoFactory factory;

    public void returnBookInTime(ReturnBookInput input) {
        Connection connection = factory.getConnection();
        try {
            connection.setAutoCommit(false);
            AbonnementDao dao = factory.createJdbcAbonnementDao(connection);
            dao.returnBook(input);


            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void returnWithFine(User user, ReturnBookInput input) {
        Connection connection = factory.getConnection();
        try {
            connection.setAutoCommit(false);
            AbonnementDao dao = factory.createJdbcAbonnementDao(connection);
            dao.payAndReturnBook(user, input);


            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    private static class Holder {
        private static final AbonnementService INSTANCE = new AbonnementService(DaoFactory.getInstance());
    }

    public static AbonnementService getInstance() {
        return AbonnementService.Holder.INSTANCE;

    }

    AbonnementService(DaoFactory factory) {
        this.factory = factory;
//        this.userDetailsService = userDetailsService;
    }

    public Optional<Abonnement> getById(int id) {
        Optional<Abonnement> result = Optional.empty();
        try (Connection connection = factory.getConnection()) {
            AbonnementDao dao = factory.createJdbcAbonnementDao(connection);
            result = dao.find(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return result;

    }

    private void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

}
