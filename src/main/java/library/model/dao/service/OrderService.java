package library.model.dao.service;

import library.model.dao.DaoFactory;
import library.model.dao.OrderDao;
import library.model.dao.entity.Book;
import library.model.dao.entity.Order;
import library.model.dao.entity.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class OrderService {
    private DaoFactory factory;

    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }


    private int maxPage;

    public Optional<List<Order>> findPageOfOrders(int page, int ordersOnPage) {
        Optional<List<Order>> list = Optional.empty();
        try (Connection connection = factory.getConnection()) {
            OrderDao dao = factory.createJdbcOrderDao(connection);
            list = dao.findPageOfOrders(page, ordersOnPage, this);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    public void approveOrder(int userId, int bookId) {
        Connection connection = factory.getConnection();
        try {
            connection.setAutoCommit(false);
            OrderDao orderDao = factory.createJdbcOrderDao(connection);
            Order order = orderDao.delete(userId);
            orderDao.approveOrder(order);
            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }


    private static class Holder {
        private static final OrderService INSTANCE = new OrderService(DaoFactory.getInstance());
    }

    public static OrderService getInstance() {
        return OrderService.Holder.INSTANCE;

    }

    OrderService(DaoFactory factory) {
        this.factory = factory;

    }

    public Order createOrder(Book book, Date desireDate, User user) {
        Connection connection = factory.getConnection();
        Order order = new Order.Builder()
                .setBook(book)
                .setUser(user)
                .setDesiredDate(desireDate)
                .build();
        try {
            connection.setAutoCommit(false);
            OrderDao orderDao = factory.createJdbcOrderDao(connection);
            order = orderDao.insert(order);


            connection.commit();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            rollback(connection);
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return order;
    }

    private void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
