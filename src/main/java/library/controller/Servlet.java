package library.controller;


import library.controller.command.Command;
import library.controller.command.CommandFactoryFactory;
import library.controller.command.get.GetCommandFactory;
import library.controller.command.post.PostCommandFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet("/controller")
public class Servlet extends HttpServlet {
public static final String CONTROLLER="controller?command=";
    private static final Logger LOGGER = Logger.getLogger(Servlet.class.getName());
    private final CommandFactoryFactory factory = CommandFactoryFactory.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req.getRequestURI().toString());
        GetCommandFactory commandFactory = factory.getGetCommandFactory();
        Command command = commandFactory.defineCommand(req);
        String page = command.execute(req, resp);
//
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
        LOGGER.info("forwarding" + page);
        try {
            dispatcher.forward(req, resp);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PostCommandFactory commandFactory = factory.getPostCommandFactory();
        Command command = commandFactory.defineCommand(req);
        StringBuilder next = new StringBuilder(CONTROLLER);
                next.append(command.execute(req, resp));


//        req.getSession().setAttribute("command", next);
        LOGGER.info("Redirecting to " + next);
        resp.sendRedirect(next.toString());


    }


}

