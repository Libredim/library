package library.controller.filter;

import library.controller.command.CommandName;
import library.controller.command.Page;
import library.model.dao.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class BanFilter implements Filter {
    Set<String> set = new HashSet<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        set = new HashSet<>();
        set.add(CommandName.ABONNEMENT);
        set.add(CommandName.POST_LOGOUT);
        set.add(CommandName.INDEX);
//        set.add(null);
        set.add(CommandName.RETURN_BOOK_FROM_ABONEMENT_IN_TIME);
        set.add(CommandName.PROFILE);
        set.add(CommandName.GET_TOP_UP);
        set.add(CommandName.TOP_UP);
        set.add(CommandName.PAY_AND_RETURN);


    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LOGGER.info("ban filter");
        HttpServletRequest req = (HttpServletRequest) request;
        if (checkBan(req)) {
            if (set.contains(req.getParameter("command"))) {
                chain.doFilter(request, response);
            } else request.getServletContext().getRequestDispatcher(Page.INDEX_JSP).forward(request, response);
        }

        chain.doFilter(request, response);

    }

    private boolean checkBan(HttpServletRequest req) {

        User user = (User) req.getSession().getAttribute("user");
        if (user == null) return false;


        return user.isBan();
    }


    @Override
    public void destroy() {

    }
}
