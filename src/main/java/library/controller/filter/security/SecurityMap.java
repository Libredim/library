package library.controller.filter.security;

import library.controller.command.CommandName;
import library.model.dao.entity.Role;

import java.util.*;


public class SecurityMap {
    private final Map<String, Set<String>> map;

    public SecurityMap() {
//       USER COMMANDS
        map = new HashMap<>();
        Set<String> commands = new HashSet<>();
        commands.add(CommandName.ORDER);
        commands.add(CommandName.ABONNEMENT);
        commands.add(CommandName.POST_LOGOUT);
        commands.add(CommandName.BOOK);
        commands.add(CommandName.CATALOG);
        commands.add(CommandName.INDEX);
//        commands.add(null);
        commands.add(CommandName.POST_ORDER);
        commands.add(CommandName.RETURN_BOOK_FROM_ABONEMENT_IN_TIME);
        commands.add(CommandName.PROFILE);
        commands.add(CommandName.GET_TOP_UP);
        commands.add(CommandName.TOP_UP);
        commands.add(CommandName.PAY_AND_RETURN);
        commands.add(CommandName.TAKE_TO_READING_ROOM);
        commands.add(CommandName.SEARCH);
        map.put(Role.USER.toString(), commands);


        //GUEST
        commands = new HashSet<>();
        commands.add(CommandName.BOOK);
        commands.add(CommandName.CATALOG);
        commands.add(CommandName.SUBSCRIBE);
        commands.add(CommandName.LOGIN);
        commands.add(CommandName.POST_LOGIN);
        commands.add(CommandName.POST_SUBSCRIBE);
        commands.add(CommandName.INDEX);
//        commands.add(null);
        map.put(Role.GUEST.toString(), commands);
        commands.add(CommandName.SEARCH);
//        ADMIN
        commands = new HashSet<>();
        commands.add(CommandName.BOOK);
        commands.add(CommandName.CATALOG);
        commands.add(CommandName.DELETE_BOOK);
        commands.add(CommandName.EDIT_BOOK_PAGE);
        commands.add(CommandName.ADD_AUTHOR);
        commands.add(CommandName.INDEX);
//        commands.add(null);
        commands.add(CommandName.USERS_PAGE);
        commands.add(CommandName.PROFILE);
        commands.add(CommandName.GET_LIBRARIAN_CREATE_PAGE);
        commands.add(CommandName.BAN_USER);
        commands.add(CommandName.UNBAN_USER);
        commands.add(CommandName.DELETE_LIBRARIAN);
        commands.add(CommandName.EDIT_BOOK);
        commands.add(CommandName.CREATE_BOOK_PAGE);
        commands.add(CommandName.CREATE_BOOK);
        commands.add(CommandName.SEARCH);
        map.put(Role.ADMIN.toString(), commands);
//          LIBRARIAN
        commands = new HashSet<>();
        commands.add(CommandName.BOOK);
        commands.add(CommandName.CATALOG);
        commands.add(CommandName.SUBSCRIBE);
        commands.add(CommandName.LOGIN);
        commands.add(CommandName.POST_LOGOUT);
        commands.add(CommandName.POST_LOGIN);
        commands.add(CommandName.POST_SUBSCRIBE);
        commands.add(CommandName.INDEX);
//        commands.add(null);
        commands.add(CommandName.USERS_PAGE);
        commands.add(CommandName.ORDERS_PAGE);
        commands.add(CommandName.PROFILE);
        commands.add(CommandName.SEARCH);

        map.put(Role.LIBRARIAN.toString(), commands);
    }

    public Map<String, Set<String>> getMap() {
        return map;
    }
}
