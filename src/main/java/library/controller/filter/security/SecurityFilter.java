package library.controller.filter.security;


import javax.servlet.ServletRequest;


public interface SecurityFilter {


    public boolean check(ServletRequest request);
}
