package library.controller.filter;

import library.controller.command.Page;
import library.controller.filter.security.SecurityFilter;
import library.controller.filter.security.SecurityMap;
import library.model.dao.entity.Role;
import library.model.dao.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class RoleFilter implements Filter, SecurityFilter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LOGGER.info("Role filter");
        if (!check(request)) {
            LOGGER.info("Role is incorrect");
            request.getServletContext().getRequestDispatcher(Page.INDEX_JSP).forward(request, response);
        }
        LOGGER.info("Role is correct");
        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {

    }

    @Override
    public boolean check(ServletRequest request) {
        Map<String, Set<String>> map = new SecurityMap().getMap();

        HttpServletRequest req = (HttpServletRequest) request;
        User user = (User) req.getSession().getAttribute("user");
        String command = req.getParameter("command");
        if (user == null) {
            String role = Role.GUEST.toString();
            LOGGER.info(role + " " + command);
            LOGGER.info(String.valueOf(map.get(role)));
            LOGGER.info(String.valueOf(map.get(role).contains(command)));
            return map.get(role).contains(command);
        } else {
            List<String> roles = user.getRole();
            for (String role : roles) {
                if (map.get(role).contains(command)) {
                    return true;
                }
            }
        }
        return false;
    }

}
