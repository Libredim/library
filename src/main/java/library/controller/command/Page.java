package library.controller.command;

public class Page {
    public static final String INDEX_JSP = "/index.jsp";
    public static final String LOGIN_JSP = "/WEB-INF/jsp/login.jsp";
    public static final String SUBSCRIPTION_JSP = "/WEB-INF/jsp/subscribe.jsp";
    public static final String CATALOG_JSP = "/WEB-INF/jsp/catalog.jsp";
    public static final String BOOK_JSP = "/WEB-INF/jsp/book.jsp";
    public static final String ORDER = "/WEB-INF/jsp/order.jsp";
    public static final String ABONNEMENT = "/WEB-INF/jsp/abonnement.jsp";
    public static final String USERS_PAGE = "/WEB-INF/jsp/usersPage.jsp";
    public static final String ORDERS = "/WEB-INF/jsp/orders.jsp";
    public static final String PROFILE_PAGE = "/WEB-INF/jsp/profile.jsp";
    public static final String TOP_UP_PAGE = "/WEB-INF/jsp/topUpPage.jsp";
    public static final String LIBRARIAN_CREATE = "/WEB-INF/jsp/librarianCreate.jsp";
    public static final String EDIT_BOOK = "/WEB-INF/jsp/editBook.jsp";
    public static final String CREATE_BOOK = "/WEB-INF/jsp/createBook.jsp";
    public static final String SEARCH_PAGE = "/WEB-INF/jsp/search.jsp";
}
