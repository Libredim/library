package library.controller.command;

import library.controller.command.get.GetCommandFactory;
import library.controller.command.post.PostCommandFactory;

public class CommandFactoryFactory {

    private static class Holder {
        private static final CommandFactoryFactory INSTANCE = new CommandFactoryFactory();
    }

    public static CommandFactoryFactory getInstance() {
        return CommandFactoryFactory.Holder.INSTANCE;

    }

    public GetCommandFactory getGetCommandFactory() {
        return GetCommandFactory.getInstance();
    }

    public PostCommandFactory getPostCommandFactory() {
        return PostCommandFactory.getInstance();
    }
}
