package library.controller.command;

public class CommandName {
    public static final String INDEX = "empty";
    public static final String LOGIN = "goLogin";
    public static final String SUBSCRIBE = "doSubscribe";
    public static final String CATALOG = "catalog";
    public static final String BOOK = "book";
    public static final String ORDER = "order";
    public static final String ABONNEMENT = "abonnement";
    public static final String POST_SUBSCRIBE = "subscribe";
    public static final String POST_LOGIN = "login";
    public static final String POST_LOGOUT = "logout";
    public static final String POST_ORDER = "orderBookToAbonement";
    public static final String USERS_PAGE = "usersPage";
    public static final String ORDERS_PAGE = "orders";
    public static final String APPROVE_ORDER = "approveOrder";
    public static final String RETURN_BOOK_FROM_ABONEMENT_IN_TIME = "returnBookFromAbonement";
    public static final String PROFILE = "profile";
    public static final String GET_TOP_UP = "topUpPage";
    public static final String TOP_UP = "topUp";
    public static final String PAY_AND_RETURN = "payFine";
    public static final String GET_LIBRARIAN_CREATE_PAGE = "librarianCreation";
    public static final String BAN_USER = "banUser";
    public static final String UNBAN_USER = "unbanUser";
    public static final String DELETE_LIBRARIAN = "deleteLibrarian";
    public static final String DELETE_BOOK = "deleteBook";
    public static final String EDIT_BOOK_PAGE = "editBookPage";
    public static final String ADD_AUTHOR = "addAuthor";
    public static final String EDIT_BOOK = "editBook";
    public static final String CREATE_BOOK_PAGE = "createBookPage";
    public static final String CREATE_BOOK = "createBook";
    public static final String TAKE_TO_READING_ROOM = "takeToReadingRoom";
    public static final String SEARCH = "search";


}
