package library.controller.command.get;

import library.controller.command.Command;
import library.error.Error;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public interface GetCommand extends Command {


    default void checkErrors(HttpServletRequest request) {
        HttpSession session = request.getSession();
        List<Error> list = (List<Error>) session.getAttribute("errors");
        if (list != null) {
            request.setAttribute("errors", list);
            session.removeAttribute("errors");
        }

    }

}
