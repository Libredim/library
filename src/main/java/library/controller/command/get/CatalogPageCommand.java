package library.controller.command.get;

import library.controller.command.Page;
import library.controller.dto.CatalogPageInput;
import library.model.dao.entity.Book;
import library.model.dao.service.BookService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class CatalogPageCommand implements GetCommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        BookService service = BookService.getInstance();
        CatalogPageInput input = getInput(request);
        System.out.println(input.toString());
        List<Book> books = service.findPageOfBooks(input).get();
        books.forEach(x -> LOGGER.info(x.toString()));

        request.setAttribute("BookList", books);
        request.setAttribute("currPage", input.getPage());
        int maxPage;
        request.setAttribute("maxPage", service.getMaxPage());

        request.setAttribute("bookOnPage", input.getBooksOnPage());
        request.setAttribute("previousSort", input.getKey());

        return Page.CATALOG_JSP;
    }

    private CatalogPageInput getInput(HttpServletRequest req) {
        return new CatalogPageInput.Builder()
                .setPage(req.getParameter("page"))
                .setBooksOnPage(req.getParameter("bookOnPage"))
                .setKey(req.getParameter("sort")).build();
    }

}
