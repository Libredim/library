package library.controller.command.get;


import library.controller.command.Command;
import library.controller.command.CommandName;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class GetCommandFactory {


    public static final String COMMAND = "command";

    private static class Holder {
        private static final GetCommandFactory INSTANCE = new GetCommandFactory();

    }

    public static GetCommandFactory getInstance() {
        return GetCommandFactory.Holder.INSTANCE;

    }

    private final HashMap<String, GetCommand> commandList;

    private GetCommandFactory() {
        commandList = new HashMap<>();
        commandList.put(CommandName.LOGIN, new LoginPageCommand());
        commandList.put(CommandName.CATALOG, new CatalogPageCommand());
        commandList.put(CommandName.SUBSCRIBE, new SubscribePageCommand());
        commandList.put(CommandName.INDEX, new EmptyGetCommand());
        commandList.put(CommandName.BOOK, new BookPageCommand());
        commandList.put(CommandName.ORDER, new OrderPageCommand());
        commandList.put(CommandName.ABONNEMENT, new AbonnementPageCommand());
        commandList.put(CommandName.USERS_PAGE, new UsersPageCommand());
        commandList.put(CommandName.ORDERS_PAGE, new OrdersPageCommand());
        commandList.put(CommandName.PROFILE, new GetProfileCommand());
        commandList.put(CommandName.GET_TOP_UP, new TopUpPageCommand());
        commandList.put(CommandName.GET_LIBRARIAN_CREATE_PAGE, new CreateLibrarianCommand());
        commandList.put(CommandName.EDIT_BOOK_PAGE, new EditBookPageCommand());
        commandList.put(CommandName.CREATE_BOOK_PAGE, new CreateBookPageCommand());
        commandList.put(CommandName.SEARCH, new SearchCommand());
    }

    public Command defineCommand(HttpServletRequest req) {
        LOGGER.info("get Commands");
        Command emptyCommand = commandList.get(CommandName.INDEX);
        Command current;
        String action;

        action = req.getParameter(COMMAND);
        if (action == null || action.isEmpty()) {
            current = emptyCommand;
            LOGGER.info("No command param found");
            if (checkRedirect(req)) {
                action = req.getSession().getAttribute(COMMAND).toString();
                current = commandList.get(action);
                LOGGER.info("Command redirect " + current + " returned");
                return current;
            }
        } else if ((current = commandList.get(action)) == null) {
            current = emptyCommand;
            LOGGER.info("Command " + action + " not found");
        } else {
            current = commandList.get(action);
        }
        LOGGER.info("Command " + current + " returned");
        return current;
    }


    public Command getCommand(String name) {
        return commandList.get(name);
    }

    boolean checkRedirect(HttpServletRequest req) {
        LOGGER.info("start search");
        Enumeration<String> temp = req.getSession().getAttributeNames();
        while (temp.hasMoreElements()) {

            if (temp.nextElement().contains(COMMAND)) {
                return true;
            }
        }
        return false;
    }

}
