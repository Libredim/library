package library.controller.command.get;

import library.controller.command.Page;
import library.model.dao.entity.Book;
import library.model.dao.service.BookService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OrderPageCommand implements GetCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        if (request.getParameterMap().containsKey("bookID")) {

            BookService service = library.model.dao.service.BookService.getInstance();
            Book book = service.findBook(Integer.parseInt(request.getParameter("bookID"))).get();
            request.setAttribute("book", book);
        }

        return Page.ORDER;
    }
}
