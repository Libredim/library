package library.controller.command.get;


import library.controller.Servlet;
import library.controller.command.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;

public class LoginPageCommand implements GetCommand {
    private static final Logger LOGGER = Logger.getLogger(Servlet.class.getName());

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        checkErrors(request);
        return Page.LOGIN_JSP;
    }


}
