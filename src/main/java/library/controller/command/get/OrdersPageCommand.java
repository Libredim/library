package library.controller.command.get;

import library.controller.command.Page;
import library.model.dao.entity.Order;
import library.model.dao.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class OrdersPageCommand implements GetCommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        OrderService service = OrderService.getInstance();
        int page = setPage(request);
        int ordersOnPage = setOrdersOnPage(request);
        List<Order> orders = service.findPageOfOrders(page, ordersOnPage).get();
        request.getSession().setAttribute("orders", orders);
        request.setAttribute("currPage", page);
        request.setAttribute("maxPage", service.getMaxPage());
        request.setAttribute("ordersOnPage", ordersOnPage);
        return Page.ORDERS;
    }

    private int setPage(HttpServletRequest req) {
        String page = req.getParameter("page");
        return page == null ? 1 : Integer.parseInt(page);
    }

    private int setOrdersOnPage(HttpServletRequest req) {
        String temp = req.getParameter("ordersOnPage");
        if (temp == null) {
            return 3;
        } else {
            return Integer.parseInt(temp);
        }
    }
}
