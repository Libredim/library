package library.controller.command.get;

import library.controller.command.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SubscribePageCommand implements GetCommand {

    public String execute(HttpServletRequest request, HttpServletResponse response) {

        checkErrors(request);
        return Page.SUBSCRIPTION_JSP;
    }
}
