package library.controller.command.get;

import library.controller.command.Page;
import library.model.dao.entity.Book;
import library.model.dao.service.BookService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SearchCommand implements GetCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        BookService service = BookService.getInstance();

        List<Book> list = service.search(getSearch(request)).get();

        request.setAttribute("bookList", list);
        return Page.SEARCH_PAGE;
    }

    private String getSearch(HttpServletRequest req) {
        return "%" + req.getParameter("search") + "%";
    }
}
