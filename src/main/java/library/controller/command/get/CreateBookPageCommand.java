package library.controller.command.get;

import library.controller.command.Page;
import library.model.dao.entity.Author;
import library.model.dao.entity.Publisher;
import library.model.dao.service.AuthorService;
import library.model.dao.service.PublisherService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class CreateBookPageCommand implements GetCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        PublisherService publisherService = PublisherService.getInstance();
        AuthorService authorService = AuthorService.getInstance();
        List<Publisher> publishers = publisherService.findAll().get();
        List<Author> authors = authorService.findAll().get();


        request.setAttribute("authors", authors);
        request.setAttribute("publishers", publishers);
        return Page.CREATE_BOOK;
    }
}
