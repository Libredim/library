package library.controller.command.get;

import library.controller.command.Page;
import library.model.dao.entity.Abonnement;
import library.model.dao.entity.User;
import library.model.dao.service.AbonnementService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AbonnementPageCommand implements GetCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        AbonnementService service = AbonnementService.getInstance();
        User user = getUserFromRequest(request);
        Abonnement abonnement = service.getById(user.getId()).get();
        request.setAttribute("abonnement", abonnement);
        System.out.println(abonnement.toString());
        return Page.ABONNEMENT;
    }
}
