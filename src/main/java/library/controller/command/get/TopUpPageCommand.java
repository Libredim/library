package library.controller.command.get;

import library.controller.command.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TopUpPageCommand implements GetCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return Page.TOP_UP_PAGE;
    }
}
