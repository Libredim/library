package library.controller.command.get;

import library.controller.Servlet;
import library.controller.command.Page;
import library.model.dao.entity.User;
import library.model.dao.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.logging.Logger;

public class UsersPageCommand implements GetCommand {
    private static final Logger LOGGER = Logger.getLogger(UsersPageCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserService service = UserService.getInstance();
        int page = setPage(request);
        int usersOnPage = setUsersOnPage(request);
        List<User> users = service.findPageOfUsers(page, usersOnPage).get();

        request.getSession().setAttribute("users", users);
        request.setAttribute("currPage", request.getParameter("page"));
        request.setAttribute("maxPage", service.getMaxPage());
        request.setAttribute("usersOnPage", request.getParameter("usersOnPage"));
        return Page.USERS_PAGE;

    }

    private int setPage(HttpServletRequest req) {
        String temp = req.getParameter("page");
        if (temp == null) {
            return 1;
        } else return Integer.parseInt(temp);
    }

    private int setUsersOnPage(HttpServletRequest req) {
        String temp = req.getParameter("usersOnPage");
        if (temp == null) {
            return 3;
        } else return Integer.parseInt(temp);
    }
}
