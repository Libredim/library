package library.controller.command.post;

import library.controller.command.CommandName;
import library.controller.dto.ReturnBookInput;
import library.model.dao.entity.User;
import library.model.dao.service.AbonnementService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PayAndReturnBook implements PostCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        ReturnBookInput input = getInput(request);
        User user = getUserFromRequest(request);
        AbonnementService service = AbonnementService.getInstance();
        service.returnWithFine(user, input);
        return CommandName.ABONNEMENT;

    }

    private ReturnBookInput getInput(HttpServletRequest req) {
        return new ReturnBookInput.Builder()
                .setUserId(Integer.parseInt(req.getParameter("userID")))
                .setBookId(Integer.parseInt(req.getParameter("bookID")))
                .setExpDate(req.getParameter("expDate"))
                .setStartDate(req.getParameter("startDate"))
                .setStatus(req.getParameter("status"))
                .setFine(req.getParameter("fine"))
                .build();
    }
}
