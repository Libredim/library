package library.controller.command.post;

import library.controller.command.CommandName;
import library.model.dao.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteLibrarianCommand implements PostCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserService service = UserService.getInstance();
        System.out.println(request.getParameter("userId"));
        service.deleteLibrarian(Integer.parseInt(request.getParameter("userId")));
        return CommandName.USERS_PAGE;
    }
}
