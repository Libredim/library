package library.controller.command.post;

import library.controller.command.CommandName;
import library.model.dao.entity.Book;
import library.model.dao.entity.Order;
import library.model.dao.entity.User;
import library.model.dao.service.BookService;
import library.model.dao.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderBookToAbonement implements PostCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        OrderService orderService = OrderService.getInstance();
        BookService bookService = BookService.getInstance();
        User user = getUserFromRequest(request);
        Book book = bookService.findBook(Integer.parseInt(request.getParameter("bookID"))).get();
        Date desiredDate = null;
        try {
            desiredDate = dateFormat.parse(request.getParameter("expDate"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Order order = orderService.createOrder(book, desiredDate, user);


        return CommandName.ORDER;
    }
}
