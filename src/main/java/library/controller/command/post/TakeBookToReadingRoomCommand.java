package library.controller.command.post;

import library.controller.command.CommandName;
import library.model.dao.entity.User;
import library.model.dao.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TakeBookToReadingRoomCommand implements PostCommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        User user = getUserFromRequest(request);
        int bookId = Integer.parseInt(request.getParameter("bookId"));
        UserService service = UserService.getInstance();
        service.takeBookToReadingRoom(user, bookId);
        return CommandName.CATALOG;
    }
}
