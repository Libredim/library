package library.controller.command.post;

import library.controller.command.CommandName;
import library.controller.dto.AuthorInput;
import library.controller.validation.ValidatorFactory;
import library.controller.validation.validator.AuthorValidator;
import library.error.Error;
import library.model.dao.service.AuthorService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AddAuthorCommand implements PostCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        ValidatorFactory validatorFactory = ValidatorFactory.getInstance();
        AuthorValidator validator = validatorFactory.createAuthorValidator();
        AuthorInput input = new AuthorInput();
        input.setName(request.getParameter("name"));
        List<Error> errors = validator.validate(input);
        if (errors.isEmpty()) {
            AuthorService service = AuthorService.getInstance();
            service.addAuthor(input);
        }
        return CommandName.PROFILE;
    }
}
