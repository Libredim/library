package library.controller.command.post;

import library.controller.command.CommandName;
import library.controller.dto.ReturnBookInput;
import library.model.dao.service.AbonnementService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ReturnBookInTimeCommand implements PostCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        AbonnementService service = AbonnementService.getInstance();
        ReturnBookInput input = getInput(request);
        service.returnBookInTime(input);

        return CommandName.ABONNEMENT;
    }


    private ReturnBookInput getInput(HttpServletRequest req) {
        return new ReturnBookInput.Builder()
                .setUserId(Integer.parseInt(req.getParameter("userID")))
                .setBookId(Integer.parseInt(req.getParameter("bookID")))
                .setExpDate(req.getParameter("expDate"))
                .setStartDate(req.getParameter("startDate"))
                .setStatus(req.getParameter("status"))
                .setFine(req.getParameter("fine"))
                .build();
    }

}
