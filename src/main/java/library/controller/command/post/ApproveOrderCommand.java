package library.controller.command.post;

import library.controller.command.CommandName;
import library.model.dao.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ApproveOrderCommand implements PostCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        OrderService service = OrderService.getInstance();
        service.approveOrder(Integer.parseInt(request.getParameter("userID")), Integer.parseInt(request.getParameter("bookID")));


        return CommandName.ORDERS_PAGE;
    }
}
