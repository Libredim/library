package library.controller.command.post;


import library.controller.command.CommandName;
import library.controller.dto.SubscribeInput;
import library.controller.validation.ValidatorFactory;
import library.controller.validation.validator.SubscribeValidator;
import library.error.Error;
import library.model.dao.entity.Role;
import library.model.dao.entity.User;
import library.model.dao.exceptions.EmailOccupiedException;
import library.model.dao.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SubscribeCommand implements PostCommand {
    private final UserService userService = UserService.getInstance();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        ValidatorFactory validatorFactory = ValidatorFactory.getInstance();

        String page;
        SubscribeInput input = extractInput(request);
        System.out.println(input);
        SubscribeValidator validator = validatorFactory.createSubscribeValidator();
        List<Error> errors = validator.validate(input);
        errors.forEach(x -> System.out.println(x.getMessage()));

        if (errors.isEmpty()) {
            try {
                User user = userService.saveUser(input);
//                request.setAttribute("createdUser", user);
                if (user.getRole().contains(Role.LIBRARIAN.toString())) {
                    page = CommandName.PROFILE;
                } else {
                    page = CommandName.LOGIN;
                }

            } catch (EmailOccupiedException e) {
                System.out.println("catch");
                User user = e.getUser();
                request.setAttribute("email", user.getEmail());
                errors.add(Error.EMAIL_OCCUPIED);
                request.setAttribute("errors", errors);
                if (user.getRole().contains(Role.LIBRARIAN.toString())) {
                    page = CommandName.GET_LIBRARIAN_CREATE_PAGE;
                } else {
                    page = CommandName.SUBSCRIBE;
                }
            }

        } else {
//            request.setAttribute("email", input.getMail());
//            request.setAttribute("errors", errors);
//            request.setAttribute("emailError", ValidationError.EMAIL);
//            request.setAttribute("passwordError", ValidationError.PASSWORD);
//            request.setAttribute("passwordEqualityError", ValidationError.PASSWORD_EQUALITY);

            if (input.getRole().equals(Role.LIBRARIAN.toString())) {
                page = CommandName.GET_LIBRARIAN_CREATE_PAGE;
            } else {
                page = CommandName.SUBSCRIBE;
            }

        }
        request.getSession().setAttribute("errors", errors);
        return page;

    }


    private SubscribeInput extractInput(HttpServletRequest request) {
        return new SubscribeInput.Builder()
                .setUsername(request.getParameter("username"))
                .setFirstPassword(request.getParameter("passwordFirst"))
                .setSecondPassword(request.getParameter("passwordSecond"))
                .setMail(request.getParameter("e-mail"))
                .setRole(request.getParameter("role"))
                .build();
    }
}



