package library.controller.command.post;

import library.controller.command.CommandName;
import library.model.dao.service.BookService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteBookCommand implements PostCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        BookService service = BookService.getInstance();
        service.deleteBook(Integer.parseInt(request.getParameter("bookId")));

        return CommandName.CATALOG;
    }
}
