package library.controller.command.post;

import library.controller.command.CommandName;
import library.model.dao.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UnbanCommand implements PostCommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserService.getInstance().unbanUser(Integer.parseInt(request.getParameter("userId")));
        return CommandName.USERS_PAGE;
    }
}
