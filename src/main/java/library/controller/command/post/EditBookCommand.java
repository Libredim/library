package library.controller.command.post;

import library.controller.command.CommandName;
import library.controller.dto.BookInput;
import library.model.dao.entity.Book;
import library.model.dao.service.BookService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditBookCommand implements PostCommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        BookInput input = getInput(request);
        BookService service = BookService.getInstance();
        Book book = service.editBook(input).get();
        request.setAttribute("book", book);


        return CommandName.CATALOG;
    }

    private BookInput getInput(HttpServletRequest req) {
        return new BookInput.Builder()
                .setName(req.getParameter("bookName"))
                .setId(Integer.parseInt(req.getParameter("bookId")))
                .setAmount(Integer.parseInt(req.getParameter("amount")))
                .setAuthor(Integer.parseInt(req.getParameter("authorId")))
                .setPublisher(Integer.parseInt(req.getParameter("publisherId")))
                .setDate(req.getParameter("pubDate")).build();

    }
}
