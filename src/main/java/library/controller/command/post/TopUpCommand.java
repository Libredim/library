package library.controller.command.post;

import library.controller.command.CommandName;
import library.model.dao.entity.User;
import library.model.dao.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TopUpCommand implements PostCommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        User user = getUserFromRequest(request);
        double cash = Integer.parseInt(request.getParameter("cash"));
        UserService service = UserService.getInstance();
        try {
            service.userTopUp(user, cash);
        } catch (RuntimeException e) {
            return CommandName.TOP_UP;
        }
        user.setAccount(user.getAccount() + cash);

        return CommandName.PROFILE;
    }
}
