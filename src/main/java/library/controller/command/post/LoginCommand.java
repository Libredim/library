package library.controller.command.post;

import library.controller.command.CommandName;
import library.controller.dto.LoginInput;
import library.controller.validation.ValidatorFactory;
import library.controller.validation.validator.LoginValidator;
import library.error.Error;
import library.model.dao.entity.User;
import library.model.dao.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class LoginCommand implements PostCommand {
    private final UserService userService = UserService.getInstance();


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        ValidatorFactory validatorFactory = ValidatorFactory.getInstance();

        String page;
        LoginInput input = extractInput(request);
        LoginValidator validator = validatorFactory.createLoginValidator();
        List<Error> errors = validator.validate(input);

        if (errors.isEmpty()) {
            page = modelValidation(request, input, errors);

        } else {
            page = CommandName.LOGIN;


        }
        request.getSession().setAttribute("errors", errors);
        return page;


    }

    private String modelValidation(HttpServletRequest request, LoginInput input, List<Error> errors) {
        String page;
        Optional<User> user = userService.findUser(input.getMail());
        if (user.isPresent()) {
            if (userService.checkPassword(user.get(), input.getPassword())) {
                request.getSession().setAttribute("user", user.get());

                page = CommandName.INDEX;

            } else {

                errors.add(Error.EMAIL_PASSWORD_MISMATCH);

                request.setAttribute("mail", input.getMail());
                page = CommandName.LOGIN;

            }
        } else {
            errors.add(Error.USERNAME_NOT_FOUND);
            page = CommandName.LOGIN;

        }
        LOGGER.info(user.toString());
        return page;


    }

    private LoginInput extractInput(HttpServletRequest request) {
        return new LoginInput.Builder()
                .setMail(request.getParameter("e-mail"))
                .setPassword(request.getParameter("password"))
                .build();
    }
}
