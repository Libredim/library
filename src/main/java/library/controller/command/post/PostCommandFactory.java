package library.controller.command.post;


import library.controller.command.Command;
import library.controller.command.CommandName;
import library.controller.command.get.GetCommandFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class PostCommandFactory {


    private final HashMap<String, PostCommand> commandList;

    private static class Holder {
        private static final PostCommandFactory INSTANCE = new PostCommandFactory();

    }

    public static PostCommandFactory getInstance() {
        return PostCommandFactory.Holder.INSTANCE;

    }


    private PostCommandFactory() {
        commandList = new HashMap<>();
        commandList.put(CommandName.POST_LOGIN, new LoginCommand());

        commandList.put(CommandName.POST_SUBSCRIBE, new SubscribeCommand());
        commandList.put(CommandName.INDEX, new EmptyPostCommand());

        commandList.put(CommandName.POST_LOGOUT, new LogoutCommand());
        commandList.put(CommandName.POST_ORDER, new OrderBookToAbonement());
        commandList.put(CommandName.APPROVE_ORDER, new ApproveOrderCommand());
        commandList.put(CommandName.RETURN_BOOK_FROM_ABONEMENT_IN_TIME, new ReturnBookInTimeCommand());
        commandList.put(CommandName.TOP_UP, new TopUpCommand());
        commandList.put(CommandName.PAY_AND_RETURN, new PayAndReturnBook());
        commandList.put(CommandName.BAN_USER, new BanCommand());
        commandList.put(CommandName.UNBAN_USER, new UnbanCommand());
        commandList.put(CommandName.DELETE_LIBRARIAN, new DeleteLibrarianCommand());
        commandList.put(CommandName.DELETE_BOOK, new DeleteBookCommand());
        commandList.put(CommandName.ADD_AUTHOR, new AddAuthorCommand());
        commandList.put(CommandName.EDIT_BOOK, new EditBookCommand());
        commandList.put(CommandName.CREATE_BOOK, new CreateBookCommand());
        commandList.put(CommandName.TAKE_TO_READING_ROOM, new TakeBookToReadingRoomCommand());
    }

    public Command defineCommand(HttpServletRequest req) {
        LOGGER.info("post Commands");
        Command emptyCommand = GetCommandFactory.getInstance().getCommand("empty");
        Command current;
        String action;

        action = req.getParameter("command");
        if (action == null || action.isEmpty()) {
            current = emptyCommand;
            LOGGER.info("No command param found");
        }
        if ((current = commandList.get(action)) == null) {
            current = emptyCommand;
            LOGGER.info("Command " + action + " not found");
        }
        LOGGER.info("Command " + current + " returned");
        return current = commandList.get(action);


    }

}
