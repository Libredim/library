package library.controller.command;

import library.controller.command.get.GetCommandFactory;
import library.model.dao.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@FunctionalInterface
public interface Command {

    String execute(HttpServletRequest request, HttpServletResponse response);

    default User getUserFromRequest(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (User) session.getAttribute("user");
    }

    default String executeCommand(String commandName, HttpServletRequest request, HttpServletResponse response) {
        return GetCommandFactory.getInstance().getCommand(commandName).execute(request, response);
    }

}
