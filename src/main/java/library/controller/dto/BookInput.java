package library.controller.dto;

public class BookInput {
    private String name;
    private int id;
    private int publisher;
    private int amount;
    private int author;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPublisher() {
        return publisher;
    }

    public void setPublisher(int publisher) {
        this.publisher = publisher;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAuthor() {
        return author;
    }

    public void setAuthor(int author) {
        this.author = author;
    }

    public BookInput() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public static class Builder {
        BookInput instance = new BookInput();

        public Builder setId(int id) {
            instance.id = id;
            return this;
        }

        public Builder setAmount(int amount) {
            instance.amount = amount;
            return this;
        }

        public Builder setName(String name) {
            instance.name = name;
            return this;
        }

        public Builder setAuthor(int author) {
            instance.author = author;
            return this;
        }

        public Builder setPublisher(int publisher) {
            instance.publisher = publisher;
            return this;
        }

        public Builder setDate(String date) {
            instance.date = date;
            return this;

        }

        public BookInput build() {
            return instance;
        }

    }
}
