package library.controller.dto;

public class LoginInput {
    private String mail;
    private String password;

    private LoginInput() {

    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static class Builder {
        private LoginInput instance = new LoginInput();

        public Builder setMail(String mail) {
            instance.mail = mail;
            return this;
        }

        public Builder setPassword(String password) {
            instance.password = password;
            return this;
        }

        public LoginInput build() {
            return instance;
        }
    }

}
