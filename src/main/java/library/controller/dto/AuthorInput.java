package library.controller.dto;

public class AuthorInput {
    private String name;

    public AuthorInput() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
