package library.controller.dto;

public class SubscribeInput {
    String username;
    String mail;
    String passwordFirst;
    String passwordSecond;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    String role;

    @Override
    public String toString() {
        return "SubscribeInput{" +
                "username='" + username + '\'' +
                ", mail='" + mail + '\'' +
                ", passwordFirst='" + passwordFirst + '\'' +
                ", passwordSecond='" + passwordSecond + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPasswordFirst() {
        return passwordFirst;
    }

    public void setPasswordFirst(String passwordFirst) {
        this.passwordFirst = passwordFirst;
    }

    public String getPasswordSecond() {
        return passwordSecond;
    }

    public void setPasswordSecond(String passwordSecond) {
        this.passwordSecond = passwordSecond;
    }

    public static class Builder {
        private SubscribeInput instance = new SubscribeInput();

        public SubscribeInput.Builder setUsername(String username) {
            instance.username = username;
            return this;
        }

        public Builder setFirstPassword(String password) {
            instance.passwordFirst = password;
            return this;
        }

        public Builder setSecondPassword(String password) {
            instance.passwordSecond = password;
            return this;
        }

        public Builder setMail(String mail) {
            instance.mail = mail;
            return this;
        }

        public Builder setRole(String role) {
            instance.role = role;
            return this;
        }

        public SubscribeInput build() {
            return instance;
        }

    }
}
