package library.controller.dto;

import java.util.HashMap;
import java.util.Map;

public class CatalogPageInput {
    int page;
    int booksOnPage;
    String key;
    Map<String, String> map;

    public CatalogPageInput() {
        map = new HashMap<>();
        map.put("bookReverse", " book.name desc");
        map.put("bookDirect", " book.name asc");
        map.put("authorReverse", " book.name desc");
        map.put("authorDirect", " author.name asc");
        map.put("publisherReverse", "  publisher.name desc");
        map.put("publisherDirect", " publisher.name asc");
        map.put("dateReverse", " publication_date desc");
        map.put("dateDirect", " publication_date asc");
    }

    public int getPage() {
        return page;
    }

    @Override
    public String toString() {
        return "CatalogPageInput{" +
                "page=" + page +
                ", booksOnPage=" + booksOnPage +
                ", key='" + key + '\'' +
                ", map=" + map +
                ", " + geSql() +
                '}';
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getBooksOnPage() {
        return booksOnPage;
    }

    public void setBooksOnPage(int booksOnPage) {
        this.booksOnPage = booksOnPage;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String geSql() {
        return "SELECT book.id, book.name, publisher_id, amount, publication_date,  author_id,  publisher.name,  author.name FROM " +
                " book left JOIN publisher ON book.publisher_id=publisher.id inner JOIN author ON author_id=author.id ORDER BY " + map.get(key) + " limit ?,?";

    }

    public static class Builder {
        private CatalogPageInput instance = new CatalogPageInput();

        public Builder setPage(String page) {
            if (page == null || page.isEmpty()) instance.page = 1;
            else instance.page = Integer.parseInt(page);

            return this;
        }

        public Builder setBooksOnPage(String booksOnPage) {
            if (booksOnPage == null || booksOnPage.isEmpty()) instance.booksOnPage = 3;
            else instance.booksOnPage = Integer.parseInt(booksOnPage);
            ;
            return this;
        }

        public Builder setKey(String key) {
            if (key == null || key.isEmpty()) key = "bookDirect";

            instance.key = key;
            return this;
        }

        public CatalogPageInput build() {
            return instance;
        }
    }
}
