package library.controller.dto;


public class ReturnBookInput {
    int userId;
    int bookId;
    String expDate;
    String startDate;
    double fine;
    String status;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public double getFine() {
        return fine;
    }

    public void setFine(double fine) {
        this.fine = fine;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ReturnBookInput{" +
                "userId=" + userId +
                ", bookId=" + bookId +
                ", expDate='" + expDate + '\'' +
                ", startDate='" + startDate + '\'' +
                ", fine=" + fine +
                ", status='" + status + '\'' +
                '}';
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public static class Builder {
        private ReturnBookInput instance = new ReturnBookInput();

        public Builder setUserId(int id) {
            instance.userId = id;
            return this;
        }

        public Builder setBookId(int id) {
            instance.bookId = id;
            return this;
        }

        public Builder setExpDate(String date) {

            instance.expDate = date;
            return this;
        }

        public Builder setStartDate(String date) {

            instance.startDate = date;
            return this;
        }

        public Builder setStatus(String status) {

            instance.status = status;
            return this;
        }

        public Builder setFine(String fineStr) {

            instance.fine = Double.parseDouble(fineStr);
            return this;
        }

        public ReturnBookInput build() {
            return instance;
        }
    }
}
