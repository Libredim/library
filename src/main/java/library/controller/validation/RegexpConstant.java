package library.controller.validation;

import java.util.regex.Pattern;

public class RegexpConstant {

    private static final String EMAIL_REGEX = "^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$";
    private static final String PASSWORD_REGEX = "^[a-zA-Z0-9]\\w{3,14}$";
    private static final String USERNAME_REGEX = "^[a-zA-Zа-яА-ЯіїІЇєЄ0-9]{3,16}$";
    public static final String AUTHOR_REGEX = "^[a-zA-Zа-яА-Я\\s\\d]+$";
    public static final String BOOK_REGEX = "^[a-zA-Zа-яА-Я\\s\\d]+$";
    private static final String TEXT_REGEX = "^[a-zA-Zа-яА-Я\\s\\d]*$";


    public static final Pattern EMAIL = Pattern.compile(EMAIL_REGEX);
    public static final Pattern PASSWORD = Pattern.compile(PASSWORD_REGEX);
    public static final Pattern USERNAME = Pattern.compile(USERNAME_REGEX);
    public static final Pattern AUTHOR_NAME = Pattern.compile(AUTHOR_REGEX);
    public static final Pattern BOOK_NAME = Pattern.compile(BOOK_REGEX);
    public static final Pattern TEXT = Pattern.compile(TEXT_REGEX);

}
