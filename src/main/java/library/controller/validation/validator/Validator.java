package library.controller.validation.validator;

import library.controller.validation.rule.ValidationRule;
import library.error.Error;

import java.util.LinkedList;
import java.util.List;


public abstract class Validator<R extends ValidationRule<E>, E> {

    private List<R> rules = new LinkedList<>();

    public List<Error> validate(E inputEntity) {
        List<Error> errors = new LinkedList<>();
        for (R rule : rules) {
            if (!rule.checkRule(inputEntity)) {
                errors.add(rule.getError());
            }
        }
        return errors;
    }

    void addRule(R rule) {
        rules.add(rule);
    }
}
