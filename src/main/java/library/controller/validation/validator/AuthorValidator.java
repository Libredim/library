package library.controller.validation.validator;

import library.controller.dto.AuthorInput;
import library.controller.validation.rule.aurhor.AuthorNameRule;

public class AuthorValidator extends Validator<AuthorNameRule, AuthorInput> {
    private static class Holder {
        private static AuthorValidator instance = new AuthorValidator();

        static {
            instance.addRule(new AuthorNameRule());


        }
    }

    public static AuthorValidator getInstance() {
        return AuthorValidator.Holder.instance;
    }

    AuthorValidator() {

    }
}
