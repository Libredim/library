package library.controller.validation.validator;

import library.controller.dto.LoginInput;
import library.controller.validation.rule.login.LoginRule;
import library.controller.validation.rule.login.MailRule;
import library.controller.validation.rule.login.PasswordRule;

public class LoginValidator extends Validator<LoginRule, LoginInput> {
    private static class Holder {
        private static LoginValidator instance = new LoginValidator();

        static {
            instance.addRule(new MailRule());
            instance.addRule(new PasswordRule());

        }
    }

    public static LoginValidator getInstance() {
        return Holder.instance;
    }

    LoginValidator() {

    }
}
