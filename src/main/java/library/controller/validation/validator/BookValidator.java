package library.controller.validation.validator;

import library.controller.dto.BookInput;
import library.controller.validation.rule.book.BookNameRule;

public class BookValidator extends Validator<BookNameRule, BookInput> {
    private static class Holder {
        private static BookValidator instance = new BookValidator();

        static {
            instance.addRule(new BookNameRule());


        }
    }

    public static BookValidator getInstance() {
        return BookValidator.Holder.instance;
    }

    BookValidator() {

    }
}
