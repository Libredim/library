package library.controller.validation.validator;

import library.controller.validation.rule.subscription.MailRule;
import library.controller.validation.rule.subscription.PasswordRule;
import library.controller.validation.rule.subscription.SamePassword;
import library.controller.validation.rule.subscription.UsernameRule;

public class SubscribeValidator extends Validator {
    private static class Holder {
        private static SubscribeValidator instance = new SubscribeValidator();

        static {
            instance.addRule(new UsernameRule());
            instance.addRule(new PasswordRule());
            instance.addRule(new SamePassword());
            instance.addRule(new MailRule());

        }
    }

    public static SubscribeValidator getInstance() {
        return SubscribeValidator.Holder.instance;
    }

    SubscribeValidator() {

    }
}
