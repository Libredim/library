package library.controller.validation;

import library.controller.validation.validator.AuthorValidator;
import library.controller.validation.validator.LoginValidator;
import library.controller.validation.validator.SubscribeValidator;

public class ValidatorFactory {
    private static ValidatorFactory instance;

    private ValidatorFactory() {
    }

    public static ValidatorFactory getInstance() {
        if (instance == null) instance = new ValidatorFactory();

        return instance;
    }


    public LoginValidator createLoginValidator() {
        return LoginValidator.getInstance();
    }

    public SubscribeValidator createSubscribeValidator() {
        return SubscribeValidator.getInstance();
    }

    public AuthorValidator createAuthorValidator() {
        return AuthorValidator.getInstance();
    }
}
