package library.controller.validation.rule.book;


import library.controller.dto.BookInput;
import library.controller.validation.rule.ValidationRule;
import library.error.Error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static library.controller.validation.RegexpConstant.BOOK_NAME;

public class BookNameRule implements ValidationRule<BookInput> {
    private Error error = Error.BOOK_NAME;
    private Pattern bookPattern = BOOK_NAME;


    @Override
    public boolean checkRule(BookInput input) {
        String name = input.getName();
        Matcher matcher = bookPattern.matcher(name);

        return matcher.matches();
    }

    @Override
    public Error getError() {
        return error;
    }
}
