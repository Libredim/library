package library.controller.validation.rule;


import library.error.Error;

public interface ValidationRule<E> {
    boolean checkRule(E input);

    Error getError();
}
