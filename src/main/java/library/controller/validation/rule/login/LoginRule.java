package library.controller.validation.rule.login;


import library.controller.dto.LoginInput;
import library.controller.validation.rule.ValidationRule;

public interface LoginRule extends ValidationRule<LoginInput> {

}
