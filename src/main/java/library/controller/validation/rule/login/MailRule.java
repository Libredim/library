package library.controller.validation.rule.login;


import library.controller.dto.LoginInput;
import library.controller.validation.RegexpConstant;
import library.error.Error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MailRule implements LoginRule {

    private Error error = Error.EMAIL;
    private Pattern mailPattern = RegexpConstant.EMAIL;

    @Override
    public boolean checkRule(LoginInput input) {
        String mail = input.getMail();
        Matcher matcher = mailPattern.matcher(mail);

        return matcher.matches();
    }

    @Override
    public Error getError() {
        return error;
    }

}
