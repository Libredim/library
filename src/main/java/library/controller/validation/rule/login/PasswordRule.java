package library.controller.validation.rule.login;


import library.controller.dto.LoginInput;
import library.controller.validation.RegexpConstant;
import library.error.Error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordRule implements LoginRule {

    Error error = Error.PASSWORD;
    private Pattern passwordPattern = RegexpConstant.PASSWORD;

    @Override
    public boolean checkRule(LoginInput input) {
        String password = input.getPassword();
        Matcher matcher = passwordPattern.matcher(password);
        return matcher.matches();
    }

    @Override
    public Error getError() {
        return error;
    }

}
