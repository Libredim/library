package library.controller.validation.rule.subscription;

import library.controller.dto.SubscribeInput;
import library.controller.validation.rule.ValidationRule;

public interface SubscriptionRule extends ValidationRule<SubscribeInput> {

}
