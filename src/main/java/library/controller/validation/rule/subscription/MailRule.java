package library.controller.validation.rule.subscription;

import library.controller.dto.SubscribeInput;
import library.controller.validation.RegexpConstant;
import library.error.Error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MailRule implements SubscriptionRule {
    private Error error = Error.EMAIL;
    private Pattern emailPattern = RegexpConstant.EMAIL;

    @Override
    public boolean checkRule(SubscribeInput input) {
        String email = input.getMail();
        Matcher matcher = emailPattern.matcher(email);

        return matcher.matches();
    }

    @Override
    public Error getError() {
        return error;
    }
}
