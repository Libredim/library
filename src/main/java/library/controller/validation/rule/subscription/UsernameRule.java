package library.controller.validation.rule.subscription;


import library.controller.dto.SubscribeInput;
import library.controller.validation.RegexpConstant;
import library.error.Error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsernameRule implements SubscriptionRule {

    private Error error = Error.USERNAME;
    private Pattern usernamePattern = RegexpConstant.USERNAME;

    @Override
    public boolean checkRule(SubscribeInput input) {
        String username = input.getUsername();
        Matcher matcher = usernamePattern.matcher(username);

        return matcher.matches();
    }

    @Override
    public Error getError() {
        return error;
    }

}
