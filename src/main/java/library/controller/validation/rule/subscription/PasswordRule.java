package library.controller.validation.rule.subscription;


import library.controller.dto.SubscribeInput;
import library.controller.validation.RegexpConstant;
import library.error.Error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordRule implements SubscriptionRule {

    Error error = Error.PASSWORD;
    private Pattern passwordPattern = RegexpConstant.PASSWORD;

    @Override
    public boolean checkRule(SubscribeInput input) {
        String password = input.getPasswordFirst();
        Matcher matcher = passwordPattern.matcher(password);
        return matcher.matches();
    }

    @Override
    public Error getError() {
        return error;
    }

}
