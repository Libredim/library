package library.controller.validation.rule.subscription;

import library.controller.dto.SubscribeInput;
import library.error.Error;

public class SamePassword implements SubscriptionRule {
    private Error error = Error.PASSWORD_EQUALITY;

    @Override
    public boolean checkRule(SubscribeInput input) {
        return input.getPasswordFirst().equals(input.getPasswordSecond());
    }

    @Override
    public Error getError() {
        return error;
    }
}
