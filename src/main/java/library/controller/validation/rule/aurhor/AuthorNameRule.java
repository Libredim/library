package library.controller.validation.rule.aurhor;

import library.controller.dto.AuthorInput;
import library.controller.validation.rule.ValidationRule;
import library.error.Error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static library.controller.validation.RegexpConstant.AUTHOR_NAME;


public class AuthorNameRule implements ValidationRule<AuthorInput> {
    private Error error = Error.AUTHOR_NAME;
    private Pattern authorPattern = AUTHOR_NAME;

    @Override
    public boolean checkRule(AuthorInput input) {
        String name = input.getName();
        Matcher matcher = AUTHOR_NAME.matcher(name);

        return matcher.matches();
    }

    @Override
    public Error getError() {
        return error;
    }
}
