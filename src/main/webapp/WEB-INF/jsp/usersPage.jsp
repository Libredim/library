<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html><%@include file="lib/bootstrapIncluds.jsp" %>
<head>


</head>
<body class="text-center">
<%@include file="lib/header.jsp" %>

hello admin

 <table border="1" cellpadding="5" cellspacing="5">


      <c:forEach var="userFromList" items="${users}">
          <tr>

             <td>${userFromList.toString()}
<c:if test="${ sessionScope.user.getRole().contains(admin)&& !userFromList.ban}">
                                        <form action="controller" method="post">
                                                                 <input type="hidden" name="command" value="banUser"/>
                                                                 <input type="hidden" name="userId" value="${userFromList.id}" />

                                                                 <button type="submit" class="btn btn-outline-light me-2">Ban</button>
                                                      </form>
                                      </c:if>
                                      <c:if test="${ sessionScope.user.getRole().contains(admin)&& userFromList.ban}">
                                                                              <form action="controller" method="post">
                                                                                                       <input type="hidden" name="command" value="unbanUser"/>
                                                                                                       <input type="hidden" name="userId" value="${userFromList.id}" />

                                                                                                       <button type="submit" class="btn btn-outline-light me-2">Lift ban</button>
                                                                                            </form>
                                                                            </c:if>
            <c:if test="${ sessionScope.user.getRole().contains(admin)&& userFromList.getRole().contains(librarian)}">
                                                                                         <form action="controller" method="post">
                                                                                                                  <input type="hidden" name="command" value="deleteLibrarian"/>
                                                                                                                  <input type="hidden" name="userId" value="${userFromList.id}" />

                                                                                                                  <button type="submit" class="btn btn-outline-light me-2">Delete Librarian</button>
                                                                                                       </form>
                                                                                       </c:if>
                                      </td>
          </tr>
      </c:forEach>

</table>

  <%--For displaying Previous link except for the 1st page --%>
  <c:if test="${currPage != 1}">


       <form action="controller" method="get">


              <input type="hidden" name="command" value="usersPage" />
                <input type="hidden" name="page" value=${currPage-1} />
                <input type="hidden" name="usersOnPage" value="${usersOnPage}" />



                <button class="btn btn-primary" type="submit">Previous</button>

              </form>

  </c:if>

  <%--For displaying Page numbers. The when condition does not display
              a link for the current page--%>


          <c:forEach begin="1" end="${maxPage}" var="i">
              <c:choose>
                  <c:when test="${currPage eq i}">
                      <td>${i}</td>
                  </c:when>
                  <c:otherwise>
                       <form action="controller" method="get">
                      <input type="hidden" name="command" value="usersPage" />
                                     <input type="hidden" name="page" value="${i}" />
                                     <input type="hidden" name="usersOnPage" value=${usersOnPage} />


                                     <button class="btn btn-primary" type="submit">${i}</button>
                                   </form>
                  </c:otherwise>
              </c:choose>
          </c:forEach>


  <%--For displaying Next link --%>

  <c:if test="${currPage lt maxPage}">
      <form action="controller" method="get">


                    <input type="hidden" name="command" value="usersPage"/>
                      <input type="hidden" name="page" value=${currPage+1} />
                      <input type="hidden" name="usersOnPage"value=${usersOnPage} />



                      <button class="btn btn-primary" type="submit">Next</button>

                    </form>
  </c:if>

</body>
</html>