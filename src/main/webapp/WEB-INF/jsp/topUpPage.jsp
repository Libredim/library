
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html><%@include file="lib/bootstrapIncluds.jsp" %>
<head>


</head>
<body class="text-center">
<%@include file="lib/header.jsp" %>
<container>
<main class="form-signin w-100 m-auto">
      <form action="controller" method="post">

          <h1 class="h3 mb-3 fw-normal">Please write amount</h1>
        <input type="hidden" name="command" value="topUp"/>
          <div class="form-floating">
            <input type="number" name="cash" class="form-control" id="floatingInput" placeholder="Cash">

          </div>

          </div>


          <button class="btn btn-primary" type="submit">Top Up</button>

        </form>
       </main>
        </container>
        </br>
        <c:forEach var="error" items="${errors}">
                                       ${error.getMessage()}
                                       </c:forEach>



</body>
</html>

