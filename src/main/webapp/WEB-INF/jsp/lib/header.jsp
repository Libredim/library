<header class="p-3 bg-dark text-white">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="library/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
          <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"></use></svg>
        </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
          <li>
          <a class="nav-link px-2 text-secondary" href="controller?command=empty">Home</a>
                            </form></li>
         <c:set var = "userRole" scope = "session" value = "USER"/>
          <li><c:if test="${sessionScope.user.getRole().contains(userRole)}">

          <a class="nav-link px-2 text-white" href="controller?command=abonnement">Abonement</a>  </c:if>   </li>
          <c:set var = "librarian" scope = "session" value = "LIBRARIAN"/>
          <li><c:if test="${ sessionScope.user.getRole().contains(librarian)}">

            <a class="nav-link px-2 text-white" href="controller?command=orders">Orders</a>

                                                    </c:if></li>
           <c:set var = "admin" scope = "session" value = "ADMIN"/>
          <li><c:if test="${ sessionScope.user.getRole().contains(admin) || sessionScope.user.getRole().contains(librarian)}">


            <a class="nav-link px-2 text-white" href="controller?command=usersPage">View Users</a>

                                      </c:if></li>
          <li>
                           <a class="nav-link px-2 text-white" href="controller?command=catalog">Books</a>
                           </li>
                           <li><c:if test="${sessionScope.user!=null}">


                                      <a class="nav-link px-2 text-white"e href="controller?command=profile">Profile</a>
                                                                 </c:if></li>
        </ul>

        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3" role="search" action="controller" method="get">
          <input type="search" name="search" class="form-control form-control-dark text-white bg-dark" placeholder="Search..." aria-label="Search">
          <input type="hidden" name="command" value="search"/>
        </form>

        <div class="text-end">
          <c:if test="${sessionScope.user == null}">
             <form action="controller" method="get">
                 <input type="hidden" name="command" value="doSubscribe"/>
                  <button type="submit" class="btn btn-warning">Sign up</button>
             </form>

              <form action="controller" method="get">
                         <input type="hidden" name="command" value="goLogin"/>
                         <button type="submit" class="btn btn-outline-light me-2">Sign in</button>
              </form>
           </c:if>


        </div>
        <div class="text-end">
                  <c:if test="${sessionScope.user != null}">
                     hello ${sessionScope.user.getUsername()}
                                                               <form action="controller" method="post">
                                                                                        <input type="hidden" name="command" value="logout"/>
                                                                                     <button type="submit" class="btn btn-outline-light me-2">logout</button>
                                                                             </form>
</c:if>
                </div>
      </div>
    </div>

  </header>