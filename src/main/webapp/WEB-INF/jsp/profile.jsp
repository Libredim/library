<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html><%@include file="lib/bootstrapIncluds.jsp" %>
<head>


</head>
<body class="text-center">
<%@include file="lib/header.jsp" %>

${user.toString()}

          <c:if test="${sessionScope.user.getRole().contains(userRole)}">
                        <form action="controller" method="get">
                                      <input type="hidden" name="command" value="topUpPage"/>
                                      <button type="submit" class="btn btn-outline-light me-2">Top Up</button>
                                                       </form>



                                                    </c:if></br>




          <c:if test="${ sessionScope.user.getRole().contains(librarian)}">
                                                    <form action="controller" method="get">
                                                                               <input type="hidden" name="command" value="orders"/>
                                                                               <input type="hidden" name="page" value="1" />
                                                                                <input type="hidden" name="ordersOnPage" value="3"/>
                                                                               <button type="submit" class="btn btn-outline-light me-2">Orders</button>
                                                                    </form>



                                                    </c:if></br>

          <c:if test="${ sessionScope.user.getRole().contains(admin) || sessionScope.user.getRole().contains(librarian)}">
                                        <form action="controller" method="get">
                                                                 <input type="hidden" name="command" value="usersPage"/>
                                                                 <input type="hidden" name="page" value="1" />
                                                                  <input type="hidden" name="usersOnPage" value="3"/>
                                                                 <button type="submit" class="btn btn-outline-light me-2">View Users</button>
                                                      </form>



                                      </c:if>
<c:if test="${ sessionScope.user.getRole().contains(admin)}">
                                        <form action="controller" method="get">
                                                                 <input type="hidden" name="command" value="librarianCreation"/>

                                                                 <button type="submit" class="btn btn-outline-light me-2">Create Librarian</button>
                                                      </form>



                                      </c:if>

      <c:if test="${ sessionScope.user.getRole().contains(admin)}">
                                              <form action="controller" method="post">
                                              Name
                                                                       <input type="hidden" name="command" value="addAuthor"/>
                                                                        <input type="Text" name="name" class="form-control" id="floatingInput" placeholder="Name">
                                                                       <button type="submit" class="btn btn-outline-light me-2">Create author</button>
                                                            </form>



                                            </c:if>
                                              <c:if test="${ sessionScope.user.getRole().contains(admin)}">
<form action="controller" method="get">

    <input type="hidden" name="command" value="createBookPage"/>

  <input type="submit" value="Create Book">

</form>
  </c:if>
</body>
</html>