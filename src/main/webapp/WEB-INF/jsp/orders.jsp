<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html><%@include file="lib/bootstrapIncluds.jsp" %>
<head>


</head>
<body class="text-center">
<%@include file="lib/header.jsp" %>

hello admin

 <table border="1" cellpadding="5" cellspacing="5">
    <th>Info</th>
              <th></th>

      <c:forEach var="orderFromList" items="${orders}">
          <tr>

             <td>${orderFromList.toString()}</td>

                <td><form action="controller" method="post">


                                  <input type="hidden" name="command" value="approveOrder" />
                                    <input type="hidden" name="userID" value=${orderFromList.getUser().id} />
                                    <input type="hidden" name="bookID" value=${orderFromList.getBook().id} />
                                     <input type="hidden" name="page" value="1" />
                                       <input type="hidden" name="ordersOnPage" value="3"/>


                                    <button class="btn btn-primary" type="submit">Approve</button>

                                  </form></td>
          </tr>
      </c:forEach>



  <%--For displaying Previous link except for the 1st page --%>
  <c:if test="${currPage != 1}">


       <form action="controller" method="get">


              <input type="hidden" name="command" value="orders" />
                <input type="hidden" name="page" value=${currPage-1} />
                <input type="hidden" name="ordersOnPage" value="${ordersOnPage}" />



                <button class="btn btn-primary" type="submit">Previous</button>

              </form>

  </c:if>

  <%--For displaying Page numbers. The when condition does not display
              a link for the current page--%>


          <c:forEach begin="1" end="${maxPage}" var="i">
              <c:choose>
                  <c:when test="${currPage eq i}">
                      <td>${i}</td>
                  </c:when>
                  <c:otherwise>
                       <form action="controller" method="get">
                      <input type="hidden" name="command" value="ordersPage" />
                                     <input type="hidden" name="page" value="${i}" />
                                     <input type="hidden" name="ordersOnPage" value=${ordersOnPage} />


                                     <button class="btn btn-primary" type="submit">${i}</button>
                                   </form>
                  </c:otherwise>
              </c:choose>
          </c:forEach>


  <%--For displaying Next link --%>

  <c:if test="${currPage lt maxPage}">
      <form action="controller" method="get">


                    <input type="hidden" name="command" value="ordersPage"/>
                      <input type="hidden" name="page" value=${currPage+1} />
                      <input type="hidden" name="ordersOnPage"value=${ordersOnPage} />



                      <button class="btn btn-primary" type="submit">Next</button>

                    </form>
  </c:if>

</body>
</html>