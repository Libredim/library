
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html><%@include file="lib/bootstrapIncluds.jsp" %>
<head>


</head>
<body class="text-center">
<%@include file="lib/header.jsp" %>

 <c:if test="${book == null}">


       <form action="controller" method="get">


              <input type="hidden" name="command" value="catalog" />
                <input type="hidden" name="page" value=${currPage-1} />
                <input type="hidden" name="bookOnPage" value="${bookOnPage}" />



                <button class="btn btn-primary" type="submit"> Go to catalog</button>

              </form>

  </c:if>

  <c:if test="${book != null}">


          <table border="1" cellpadding="5" cellspacing="5">
               <tr>
                   <th>Book ID</th>
                   <th>Book Name</th>
                   <th>Amount</th>
                    <th></th>

               </tr>

                    <tr>
                       <td>${requestScope.book.id}</td>
                       <td>${requestScope.book.name}</td>
                       <td>${requestScope.book.amount}</td>
                        <td> <form action="controller" method="get">


                                            <input type="hidden" name="command" value="order" />

                                              <input type="hidden" name="bookID" value=${requestScope.book.id} />



                                              <button class="btn btn-primary" type="submit"> Order Book</button>

                                            </form>
                                            </td>

                   </tr>
                 <c:if test="${user.bookInReadingRoom == null}">
                <form action="controller" method="Post">

                    <input type="hidden" name="command" value="takeToReadingRoom"/>
                    <input type="hidden" name="bookId" value="${book.id}"/>
                  <input type="submit" value="Take to reading room">
                  </form>
    </c:if>
           </table>


    </c:if>


</body>
</html>