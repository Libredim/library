<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html><%@include file="lib/bootstrapIncluds.jsp" %>
<head>


</head>
<body class="text-center">
<%@include file="lib/header.jsp" %>

 <table border="1" cellpadding="5" cellspacing="5">
      <tr>
          <th> Name<form action="controller" method="get">
           <input type="hidden" name="command" value="catalog"/>
                                             <input type="hidden" name="page" value="1" />
                                                 <input type="hidden" name="bookOnPage" value="3"/>
                                                  <input type="hidden" name="sort" value="bookReverse"/>
                                              <input type="submit"value="↑" />
                                         </form>
                                         <form action="controller" method="get">
                                         <input type="hidden" name="command" value="catalog"/>
                                         <input type="hidden" name="page" value="1" />
                                                                                          <input type="hidden" name="bookOnPage" value="3"/>
                                                                                           <input type="hidden" name="sort" value="bookDirect"/>
                                                                                           <input type="submit"value="↓" />

                                                                                  </form></th>
          <th>Author<form action="controller" method="get">
                         <input type="hidden" name="command" value="catalog"/>
                                                           <input type="hidden" name="page" value="1" />
                                                               <input type="hidden" name="bookOnPage" value="3"/>
                                                                <input type="hidden" name="sort" value="authorReverse"/>
                                                            <input type="submit"value="↑" />
                                                       </form>
                                                       <form action="controller" method="get">
                                                       <input type="hidden" name="page" value="1" />
                                                       <input type="hidden" name="command" value="catalog"/>
                                                                                                        <input type="hidden" name="bookOnPage" value="3"/>
                                                                                                         <input type="hidden" name="sort" value="authorDirect"/>
                                                                                                    <input type="submit"value="↓" />
                                                                                                </form></th>
          <th>Publisher<form action="controller" method="get">
                         <input type="hidden" name="command" value="catalog"/>

                                                           <input type="hidden" name="page" value="1" />
                                                               <input type="hidden" name="bookOnPage" value="3"/>
                                                                <input type="hidden" name="sort" value="publisherReverse"/>
                                                            <input type="submit"value="↑" />
                                                       </form>
                                                       <form action="controller" method="get">
                                                       <input type="hidden" name="page" value="1" />
                                                       <input type="hidden" name="command" value="catalog"/>
                                                                                                        <input type="hidden" name="bookOnPage" value="3"/>
                                                                                                         <input type="hidden" name="sort" value="publisherDirect"/>
                                                                                                     <input type="submit"value="↓" />
                                                                                                </form></th>
<th>Publication Date <form action="controller" method="get">
               <input type="hidden" name="command" value="catalog"/>
                                                 <input type="hidden" name="page" value="1" />
                                                     <input type="hidden" name="bookOnPage" value="3"/>
                                                      <input type="hidden" name="sort" value="dateReverse"/>
                                                 <input type="submit"value="↑" />
                                             </form>
                                             <form action="controller" method="get">
                                             <input type="hidden" name="command" value="catalog"/>
                                             <input type="hidden" name="page" value="1" />
                                                                                              <input type="hidden" name="bookOnPage" value="3"/>
                                                                                               <input type="hidden" name="sort" value="dateDirect"/>
                                                                                          <input type="submit"value="↓" />
                                                                                      </form></th>
      </tr>

      <c:forEach var="bookFromList" items="${BookList}">
          <tr>
              <td>${bookFromList.name}</td>
              <td>${bookFromList.author.toString()}</td>
              <td>${bookFromList.publisher.toString()}</td>
              <td>${bookFromList.publicationDate}</td>
              <td>
              <c:if test="${bookFromList.amount>0}">
              <form action="controller" method="get">


                            <input type="hidden" name="command" value="book" />
                              <input type="hidden" name="bookID" value=${bookFromList.id} />



                              <button class="btn btn-primary" type="submit">details</button>

                            </form>
                </c:if>

<c:if test="${ sessionScope.user.getRole().contains(admin)}">
                                                   <form action="controller" method="post">
                                                                  <input type="hidden" name="command" value="deleteBook"/>
                                                                  <input type="hidden" name="bookId" value="${bookFromList.id}" />

                                                                  <button type="submit" class="btn btn-outline-light me-2">Delete Book</button>
                                                                                </form>
                                                                                       </c:if>
<c:if test="${ sessionScope.user.getRole().contains(admin)}">
<form action="controller" method="get">
                                                                  <input type="hidden" name="command" value="editBookPage"/>
                                                                  <input type="hidden" name="bookId" value="${bookFromList.id}" />

                                                                  <button type="submit" class="btn btn-outline-light me-2">Edit Book</button>
                                                                                </form>
                                                                                       </c:if>
          </tr>
      </c:forEach>

  </table>

  <%--For displaying Previous link except for the 1st page --%>
  <c:if test="${currPage != 1}">


       <form action="controller" method="get">


              <input type="hidden" name="command" value="catalog" />
                <input type="hidden" name="page" value=${currPage-1} />
                <input type="hidden" name="bookOnPage" value="${bookOnPage}" />
                <input type="hidden" name="sort" value="${previousSort}" />


                <button class="btn btn-primary" type="submit">Previous</button>

              </form>

  </c:if>

  <%--For displaying Page numbers. The when condition does not display
              a link for the current page--%>


          <c:forEach begin="1" end="${maxPage}" var="i">
              <c:choose>
                  <c:when test="${currPage eq i}">
                      <td>${i}</td>
                  </c:when>
                  <c:otherwise>
                       <form action="controller" method="get">
                      <input type="hidden" name="command" value="catalog" />
                                     <input type="hidden" name="page" value="${i}" />
                                     <input type="hidden" name="bookOnPage" value=${bookOnPage} />
<input type="hidden" name="sort" value="${previousSort}" />

                                     <button class="btn btn-primary" type="submit">${i}</button>
                                   </form>
                  </c:otherwise>
              </c:choose>
          </c:forEach>


  <%--For displaying Next link --%>

  <c:if test="${currPage lt maxPage}">
      <form action="controller" method="get">


                    <input type="hidden" name="command" value="catalog"/>
                      <input type="hidden" name="page" value=${currPage+1} />
                      <input type="hidden" name="bookOnPage"value=${bookOnPage} />
<input type="hidden" name="sort" value="${previousSort}" />


                      <button class="btn btn-primary" type="submit">Next</button>

                    </form>
  </c:if>

</body>
</html>
