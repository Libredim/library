
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html><%@include file="lib/bootstrapIncluds.jsp" %>
<head>


</head>
<body class="text-center">
<%@include file="lib/header.jsp" %>


      <form action="controller" method="post">

        <input type="hidden" name="command" value="createBook" />

    <input type="text" name="bookName" class="form-control" id="floatingInput" placeholder="Name"/ >


                 <input type="number" name="amount" class="form-control" id="floatingInput" / >
                     <input type="date" id="start" name="pubDate"
                                              value="2020-07-22"
                                              min="1992-01-01" max="2024-12-31"/>


                    <label for="Authors">Choose a author:</label>
                    <select id="AuthorId" name="authorId">
                        <c:forEach var="author" items="${authors}">
                        <option value="${author.id}">${author.name}</option>
                        </c:forEach>
                    </select>

                    <label for="Authors">Choose a publisher:</label>
                                        <select id="PublisherId" name="publisherId">
                                            <c:forEach var="publisher" items="${publishers}">
                                            <option value="${publisher.id}">${publisher.name}</option>
                                            </c:forEach>
                                        </select>
                 </br>



                 <input type="submit" value="Submit">
        </form>




</body>
</html>