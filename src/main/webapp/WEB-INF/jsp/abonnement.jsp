<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html><%@include file="lib/bootstrapIncluds.jsp" %>
<head>


</head>
<body class="text-center">
<%@include file="lib/header.jsp" %>

""${abonnement.toString()}""
<c:set var = "abonementDetails" scope = "session" value = "${abonnement.getDetailsMap().values()}"/>
<table border="1" cellpadding="5" cellspacing="5">
    <th>Info</th>
              <th></th>
<c:forEach var="detail" items="${abonementDetails}">
          <tr>

             <td>${detail.toString()}</td>

                <td>
                <c:set var="status" scope="session" value ="IN_USE" />
<c:if test="${detail.getStatus().toString().equals(status)}">
                                        <form action="controller" method="post">
                                                                 <input type="hidden" name="command" value="returnBookFromAbonement"/>
                                                                  <input type="hidden" name="userID" value="${abonnement.id}"/>
                                                                   <input type="hidden" name="bookID" value="${detail.getBook().id}"/>
                                                                 <input type="hidden" name="expDate" value="${detail.expDate}"/>
                                                                 <input type="hidden" name="startDate" value="${detail.startDate}"/>
                                                                 <input type="hidden" name="fine" value="${detail.getFine()}"/>
                                                                 <input type="hidden" name="status" value="${detail.getStatus().toString()}"/>
                                                                 <button type="submit" class="btn btn-warning">Return</button>
                                                      </form></c:if>
                                                       <c:set var="status" scope="session" value ="EXPIRED" />
<c:if test="${detail.getStatus().toString().equals(status)}">
                                        <form action="controller" method="Post">
                                                                 <input type="hidden" name="command" value="payFine"/>
                                                                  <input type="hidden" name="userID" value="${abonnement.id}"/>
                                                                   <input type="hidden" name="bookID" value="${detail.getBook().id}"/>
                                                                 <input type="hidden" name="expDate" value="${detail.expDate}"/>
                                                                 <input type="hidden" name="startDate" value="${detail.startDate}"/>
                                                                 <input type="hidden" name="fine" value="${detail.getFine()}"/>
                                                                 <input type="hidden" name="status" value="${detail.getStatus().toString()}"/>
                                                                 <button type="submit" class="btn btn-warning">Pay and return</button>
                                                      </form>


                                      </c:if>


                </td>
          </tr>
      </c:forEach>
      </table>
</body>
</html>
